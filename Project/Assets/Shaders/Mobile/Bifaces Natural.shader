Shader "Somniis/Bifaces Natural" {
	Properties{
		_ColorTint ("Color", Color) = (1.0,.3,.2576,1)
		_RimPower ("Rim Power", Range(0.2,10.0)) = 3.0
	  	_MainTex ("Texture", 2D) = "white" {}
  }  
  
   	SubShader {
		Tags { "RenderType"="Opaque" }
		Pass{
			cull off
		}
		
		CGPROGRAM
		#pragma surface surf Lambert
		
		struct Input{
			fixed2 uv_MainTex;
			fixed4 uv_Effect;
		};
		
		sampler2D _MainTex;
		fixed4 _Effect;
		void surf (Input IN, inout SurfaceOutput o){
			o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb * 0.5;
			o.Emission = _Effect.rgb;
		}
		ENDCG
	}
  }
