﻿Shader "Somniis/Water" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Cor", Color) = (0,0,0,0)
	}

	Subshader {
	Tags { "Queue" = "Transparent" }
        Pass {
            Blend SrcAlpha OneMinusSrcAlpha
            Material {
                Diffuse [_Color]
            }
            Lighting On
            zwrite on
            SetTexture [_MainTex] {
                combine texture, texture * primary
            }
        }
}
}