﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// this is the Terrain Creator Engine
/// </summary>
public class TerrainCreator : MonoBehaviour
{
    public GameObject gramaVerde, gramaPodre;
    public enum Objeto { none = 0, Guerreiro, Arqueiro, Mago, TendaGuerreiro, TendaArqueiro, TendaMago, Obstaculo, Terreno };
    public enum TipoDeTerreno { gramaVerde = 0, gramaPodre };

    static public Terreno[, ,] Blocos = new Terreno[8/*coluna*/ , 8/*linha*/, 3/*altura*/];

    List<Vector2> registeredSecondFloor = new List<Vector2>();

    public struct Terreno
    {
        public TipoDeTerreno tipoDeTerreno;
        public Objeto objetoAcima;
        /// <summary>
        /// Objeto que representa o bloco na matriz
        /// </summary>
        public GameObject bloco;
        /// <summary>
        /// Objeto representando o GameObject que está em cima do atual bloco
        /// </summary>
        public GameObject gameObjectAcima;
        /// <summary>
        /// Copia dos atributos de um personagem (se houver) que está em cima do atual bloco
        /// </summary>
        public int energiaDoObjeto, campoDeVisaoDoObjeto, forcaDeAtaque, forcaDeDefesa;
        /// <summary>
        /// Posição matricial do bloco
        /// </summary>
        public Vector3 posicaoMatricial;

    }

    void Awake()
    { GenerateFirstFloor();}

    /// <summary>
    /// renomeia determinado bloco com base no seu indice.
    /// </summary>
    string NameMatrix(int coluna, int linha, int level)
    {
        string newColuna = "?";
        switch (coluna)
        {
            case 0:
                newColuna = "A";
                break;
            case 1:
                newColuna = "B";
                break;
            case 2:
                newColuna = "C";
                break;
            case 3:
                newColuna = "D";
                break;
            case 4:
                newColuna = "E";
                break;
            case 5:
                newColuna = "F";
                break;
            case 6:
                newColuna = "G";
                break;
            case 7:
                newColuna = "H";
                break;
            default:
                Debug.LogWarning("Houve um Erro na Nomeação da Matriz!");
                break;
        }

        string nome = newColuna + linha + level;
        return nome;

    }

    /// <summary>
    /// seta um parentesco de determinado objeto, com base no seu degrau para melhor organização do terreno na hierarquia.
    /// caso não haja o Tranform esperado, o objeto será armazenado neste próprio Transform
    /// </summary>
    /// <param name="level">degrau do terreno baseado no indice que inicia do 1</param>
    /// <returns>Transform ideal para armazenar o objeto</returns>
    Transform Degrau(int level)
    {
        Transform parente;
        try { parente = GameObject.Find("Level" + level).transform; }
        catch
        {
            Debug.LogWarning("O parentesco não foi encontrado, o objeto será aparentado neste transform atual!");
            parente = transform;
        }

        return parente;
    }

    /// <summary>
    /// sistema gerador do primeiro degrau de terreno
    /// </summary>
    void GenerateFirstFloor()
    {
        /*segue a seguinte regra:
         1 - os terrenos da primeira, segunda e terceira linha são constituído de grama verde;
         2 - a quarta e quinta linha têm seu terreno variavel, ou seja, pode ser tanto grama verde quanto grama podre sendo que:
            2.1 - se em determinada coluna, houver em sua quarta linha um terreno de grama podre, então nesta mesma coluna na quinta linha o terreno
            deve ser obrigatoriamente constituído de grama podre;
            2.2 - se em determinada coluna, houver em sua quarta linha um terreno de grama verde, então na quinta linha desta mesma coluna, o terreno
            pode ser tanto de grama verde quanto de grama podre;
         3 - as três ultimas linhas devem ser constituídas de grama podre;*/

        int linha = 0;
        for (int coluna = 0; coluna < 8; coluna++)
        { //Remeber: x-axis = colunas; z-axis = linhas;
            float x = 2.9f * coluna, z = 2.9f * linha;

            if (linha < 3)
            {
                Blocos[coluna, linha, 0].bloco = Instantiate(gramaVerde, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaVerde;
                Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
            }

            else if (linha == 3)
            {
                if (Random.Range(0, 7) < 3)
                {
                    Blocos[coluna, linha, 0].bloco = Instantiate(gramaPodre, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaPodre;
                    Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                    Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
                }

                else
                {
                    Blocos[coluna, linha, 0].bloco = Instantiate(gramaVerde, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaVerde;
                    Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                    Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
                }
            }

            else if (linha == 4)
            {
                if (Blocos[coluna, 3, 0].tipoDeTerreno == TipoDeTerreno.gramaPodre)
                {
                    Blocos[coluna, linha, 0].bloco = Instantiate(gramaPodre, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaPodre;
                    Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                    Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
                }

                else if (Random.Range(0, 5) < 3)
                {
                    Blocos[coluna, linha, 0].bloco = Instantiate(gramaPodre, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaPodre;
                    Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                    Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
                }

                else
                {
                    Blocos[coluna, linha, 0].bloco = Instantiate(gramaVerde, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaVerde;
                    Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                    Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
                }
            }

            else
            {
                Blocos[coluna, linha, 0].bloco = Instantiate(gramaPodre, new Vector3(x, 0, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                Blocos[coluna, linha, 0].tipoDeTerreno = TipoDeTerreno.gramaPodre;
                Blocos[coluna, linha, 0].bloco.name = NameMatrix(coluna, linha, 1);
                Blocos[coluna, linha, 0].bloco.transform.parent = Degrau(1);
                Blocos[coluna, linha, 0].objetoAcima = Objeto.none;
                Blocos[coluna, linha, 0].posicaoMatricial = new Vector3(coluna, linha, 0);
            }

            if (coluna == 7 && linha < 7)
            { coluna = -1; linha++; }
        }

        GenerateSecondFloor();
    }

    /// <summary>
    /// sistema gerador do segundo degrau de blocos.
    /// </summary>
    void GenerateSecondFloor()
    {
        /*trabalha da seguinte maneira:
         1 - para cada determinado valor da matriz do primeiro degrau, vai decidir randomicamente se vai haver ou não o segundo degrau
         2 - se houver o segundo degrau, ele deve ser igual ao bloco da mesma coluna e linha do nivel abaixo.*/
        int linha = 0;
        for (int coluna = 0; coluna < 8; coluna++)
        {
            float x = 2.9f * coluna, z = 2.9f * linha;
            if (Random.Range(0, 7) < 4)
            {
                if (Blocos[coluna, linha, 0].tipoDeTerreno == TipoDeTerreno.gramaVerde)
                {
                    Blocos[coluna, linha, 1].bloco = Instantiate(gramaVerde, new Vector3(x, 1, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 1].tipoDeTerreno = TipoDeTerreno.gramaVerde;
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.Terreno;
                    registeredSecondFloor.Add(new Vector2(coluna, linha));
                    Blocos[coluna, linha, 1].bloco.name = NameMatrix(coluna, linha, 2);
                    Blocos[coluna, linha, 1].bloco.transform.parent = Degrau(2);
                    Blocos[coluna, linha, 1].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 1].posicaoMatricial = new Vector3(coluna, linha, 1);
                }

                else
                {
                    Blocos[coluna, linha, 1].bloco = Instantiate(gramaPodre, new Vector3(x, 1, z), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 1].tipoDeTerreno = TipoDeTerreno.gramaPodre;
                    Blocos[coluna, linha, 0].objetoAcima = Objeto.Terreno;
                    registeredSecondFloor.Add(new Vector2(coluna, linha));
                    Blocos[coluna, linha, 1].bloco.name = NameMatrix(coluna, linha, 2);
                    Blocos[coluna, linha, 1].bloco.transform.parent = Degrau(2);
                    Blocos[coluna, linha, 1].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 1].posicaoMatricial = new Vector3(coluna, linha, 1);
                }
            }

            if (coluna == 7 && linha < 7)
            { coluna = -1; linha++; }
        }

        GenerateThirdFloor();
    }

    /// <summary>
    /// sistema gerador do terceiro degrau de blocos.
    /// </summary>
    void GenerateThirdFloor()
    {
        /*segue a  seguinte regra:
         1 - para cada bloco do segundo degrau de determinada linha e coluna, vai deceidir randomicamente se vai haver ou não um terceiro 
         degrau nesta mesma coluna e linha.
         2 - se houver, este vai ser o mesmo bloco da coluna e linha do degrau abaixo.*/
        foreach (Vector2 coordMatrix in registeredSecondFloor)
        {
            if (Random.Range(0, 5) < 2)
            {
                int coluna = int.Parse(coordMatrix.x.ToString()), linha = int.Parse(coordMatrix.y.ToString());
                if (Blocos[coluna, linha, 1].tipoDeTerreno == TipoDeTerreno.gramaPodre)
                {
                    Blocos[coluna, linha, 2].bloco = Instantiate(gramaPodre, new Vector3(2.9f * coordMatrix.x, 2, 2.9f * coordMatrix.y), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 2].tipoDeTerreno = TipoDeTerreno.gramaPodre;
                    Blocos[coluna, linha, 1].objetoAcima = Objeto.Terreno;
                    Blocos[coluna, linha, 2].bloco.name = NameMatrix(coluna, linha, 3);
                    Blocos[coluna, linha, 2].bloco.transform.parent = Degrau(3);
                    Blocos[coluna, linha, 2].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 2].posicaoMatricial = new Vector3(coluna, linha, 2);
                }

                else if (Blocos[coluna, linha, 1].tipoDeTerreno == TipoDeTerreno.gramaVerde)
                {
                    Blocos[coluna, linha, 2].bloco = Instantiate(gramaVerde, new Vector3(2.9f * coordMatrix.x, 2, 2.9f * coordMatrix.y), new Quaternion(0, 0, 0, 0)) as GameObject;
                    Blocos[coluna, linha, 2].tipoDeTerreno = TipoDeTerreno.gramaVerde;
                    Blocos[coluna, linha, 1].objetoAcima = Objeto.Terreno;
                    Blocos[coluna, linha, 2].bloco.name = NameMatrix(coluna, linha, 3);
                    Blocos[coluna, linha, 2].bloco.transform.parent = Degrau(3);
                    Blocos[coluna, linha, 2].objetoAcima = Objeto.none;
                    Blocos[coluna, linha, 2].posicaoMatricial = new Vector3(coluna, linha, 2);
                }
            }
        }
    }

    /// <summary>
    /// Transfere os valores de um determinado objeto que está acima de determinado bloco para o bloco
    /// </summary>
    /// <param name="ObjetoAcima">tipo de objeto que está acima do bloco</param>
    /// <param name="GameObjectPisando">GameObject que está pisando no bloco</param>
    /// <param name="energia">nível de energia do objeto</param>
    /// <param name="campoDeVisao">tamanho do campo de visao do objeto</param>
    /// <param name="forcaDeAtaque">nivel da forca de ataque do objeto</param>
    /// <param name="forcaDeDefesa">nivel da forca de defesa do objeto</param>
    /// <param name="posicaoMatriz">posicao do objeto na matriz</param>
    static public void CopyValues(Objeto ObjetoAcima, GameObject GameObjectPisando, int energia,
        int campoDeVisao, int forcaDeAtaque, int forcaDeDefesa, Vector3 posicaoMatriz)
    {
        int coluna = (int)posicaoMatriz.x, linha = (int)posicaoMatriz.y, nivel = (int)posicaoMatriz.z;

        Blocos[coluna, linha, nivel].objetoAcima = ObjetoAcima;
        Blocos[coluna, linha, nivel].gameObjectAcima = GameObjectPisando;
        Blocos[coluna, linha, nivel].energiaDoObjeto = energia;
        Blocos[coluna, linha, nivel].campoDeVisaoDoObjeto = campoDeVisao;
        Blocos[coluna, linha, nivel].forcaDeAtaque = forcaDeAtaque;
        Blocos[coluna, linha, nivel].forcaDeDefesa = forcaDeDefesa;
    }

    /// <summary>
    /// Tranfere valores de uma determinada tenda que está acima de determinado bloco para este
    /// </summary>
    /// <param name="gameObject">GameObject que está pisando no bloco</param>
    /// <param name="energia">Energia da tenda</param>
    /// <param name="posicaoMatriz">Posição matricial da tenda</param>
    static public void CopyValues(Objeto tipoDeTenda, GameObject gameObject, int energia, Vector3 posicaoMatriz)
    {
        int coluna = (int)posicaoMatriz.x, linha = (int)posicaoMatriz.y, nivel = (int)posicaoMatriz.z;

        Blocos[coluna, linha, nivel].objetoAcima = tipoDeTenda;
        Blocos[coluna, linha, nivel].gameObjectAcima = gameObject;
        Blocos[coluna, linha, nivel].energiaDoObjeto = energia;

        Blocos[coluna, linha, nivel].campoDeVisaoDoObjeto = 0;
        Blocos[coluna, linha, nivel].forcaDeAtaque = 0;
        Blocos[coluna, linha, nivel].forcaDeDefesa = 0;
    }

    /// <summary>
    /// Deleta os valores de um determinado bloco na matriz
    /// </summary>
    /// <param name="posicaoMatriz">posicao na matriz do bloco que terá seus valores deletados</param>
    static public void DeleteValues(Vector3 posicaoMatriz)
    {
        int coluna = (int)posicaoMatriz.x, linha = (int)posicaoMatriz.y, nivel = (int)posicaoMatriz.z;

        Blocos[coluna, linha, nivel].objetoAcima = Objeto.none;
        Blocos[coluna, linha, nivel].gameObjectAcima = null;
        Blocos[coluna, linha, nivel].energiaDoObjeto = 0;
        Blocos[coluna, linha, nivel].campoDeVisaoDoObjeto = 0;
        Blocos[coluna, linha, nivel].forcaDeAtaque = 0;
        Blocos[coluna, linha, nivel].forcaDeDefesa = 0;
    }

    /// <summary>
    /// Encontra determinado bloco na matriz e retorna sua posição
    /// </summary>
    /// <param name="objeto">bloco para ser procurado</param>
    /// <param name="IsInstantiateMode">faz parte do sistema de instanciação?</param>
    /// <returns>posição do bloco selecionado</returns>
    static public Vector3 FindBlockInMatrix(GameObject objeto, bool IsInstantiateMode)
    {
        int coluna = 0, linha = 0, nivel = 0;
        Vector3 posicao = new Vector3();
        try
        {
            while (objeto != Blocos[coluna, linha, nivel].bloco)
            {
                nivel++;
                if (nivel > 2)
                {
                    nivel = 0;
                    linha++;
                    if (linha > 7)
                    {
                        linha = 0;
                        coluna++;
                    }
                }
            }


            switch (IsInstantiateMode)
            {
                case true:
                    if (linha < 2)
                    {
                        if (Blocos[coluna, linha, nivel].objetoAcima == Objeto.none)
                        { posicao = new Vector3(coluna, linha, nivel); }
                        else { posicao = new Vector3(); }
                    }
                    else { posicao = new Vector3(); } 
                    break;

                case false:
                    if (Blocos[coluna, linha, nivel].objetoAcima == Objeto.none)
                    { posicao = new Vector3(coluna, linha, nivel); }
                    else { posicao = new Vector3(); } 
                    break;
            }
        }

        catch { posicao = new Vector3(); }

        return posicao;
    }

    /// <summary>
    /// Encontra determinado objeto na matriz e retorna sua posição matricial
    /// </summary>
    /// <param name="objeto">Objeto a ser procurado</param>
    /// <param name="isSellingMode">Faz parte do sistema de venda?</param>
    /// <returns>posição matricial do objeto selecionado</returns>
    static public Vector3 FindGameObjectInMatrix(GameObject objeto, bool isSellingMode)
    {
        int coluna = 0, linha = 0, nivel = 0;
        Vector3 posicao = new Vector3();
        try
        {
            while (objeto != Blocos[coluna, linha, nivel].gameObjectAcima)
            {
                nivel++;
                if (nivel > 2)
                {
                    nivel = 0;
                    linha++;
                    if (linha > 7)
                    {
                        linha = 0;
                        coluna++;
                    }
                }
            }

            if (isSellingMode)
            {
                if (Blocos[coluna, linha, nivel].objetoAcima == Objeto.Guerreiro ||
                    Blocos[coluna, linha, nivel].objetoAcima == Objeto.Arqueiro ||
                    Blocos[coluna, linha, nivel].objetoAcima == Objeto.Mago)
                { posicao = new Vector3(coluna, linha, nivel); }

                else { }
            }
            else { posicao = new Vector3(coluna, linha, nivel); }
        }

        catch { posicao = new Vector3(); }
        return posicao;
    }

    /// <summary>
    /// Transfere os valores de uma determinada posicao matricial para outra posição matricial
    /// </summary>
    /// <param name="from">Posição matricial inicial</param>
    /// <param name="to">Posição matricial final</param>
    static public void TransferValues(Vector3 from, Vector3 to)
    {
        int Tcoluna = (int)to.x, Tlinha = (int)to.y, Tnivel = (int)to.z;
        int Fcoluna = (int)from.x, Flinha = (int)from.y, Fnivel = (int)from.z;


        Blocos[Tcoluna, Tlinha, Tnivel].objetoAcima = Blocos[Fcoluna, Flinha, Fnivel].objetoAcima;
        Blocos[Tcoluna, Tlinha, Tnivel].gameObjectAcima = Blocos[Fcoluna, Flinha, Fnivel].gameObjectAcima;
        Blocos[Tcoluna, Tlinha, Tnivel].energiaDoObjeto = Blocos[Fcoluna, Flinha, Fnivel].energiaDoObjeto;
        Blocos[Tcoluna, Tlinha, Tnivel].campoDeVisaoDoObjeto = Blocos[Fcoluna, Flinha, Fnivel].campoDeVisaoDoObjeto;
        Blocos[Tcoluna, Tlinha, Tnivel].forcaDeAtaque = Blocos[Fcoluna, Flinha, Fnivel].forcaDeAtaque;
        Blocos[Tcoluna, Tlinha, Tnivel].forcaDeDefesa = Blocos[Fcoluna, Flinha, Fnivel].forcaDeDefesa;

        DeleteValues(from);
    }

    /// <summary>
    /// retorna a posicao do bloco mais alto em uma determinada posição matricial [X,Y]
    /// </summary>
    /// <param name="posicaoMatricialXY">Posição matricial a ser descoberta seu bloco mais alto</param>
    static public int CheckMaxIndexInBlock(Vector2 posicaoMatricialXY)
    {
        int indice = 0;
        while(Blocos[(int)posicaoMatricialXY.x, (int)posicaoMatricialXY.y, indice].objetoAcima != Objeto.none)
        {
            indice++;
            if(indice > 2)
            { indice = new int(); break; }
        }
        return indice;
    }
}