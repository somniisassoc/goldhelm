﻿using UnityEngine;
using System.Collections;

/// <summary>
/// this is the Enviromments Creator Engine
/// </summary>
[RequireComponent(typeof(TerrainCreator))]
public class EnvirommentsEngine : MonoBehaviour
{
    TerrainCreator.Terreno[, ,] Matriz = TerrainCreator.Blocos;
    public GameObject water, waterFall, pedra, pinheiro;
    public enum comando { Frente = 0, Trás, Direita, Esquerda }

    void Start()
    {
        int qntVezes = Random.Range(0, 2);
        for (int i = 0; i < qntVezes; i++)
        { GenerateWater(); }

        GenerateSecondaryEnviromments();
    }

    void GenerateWater()
    {
        //gera as coordenadas(bloco inicial e direção) para o instanciamento da agua, e ativa o comando para instanciar a agua
        int linha = 0;
        for (int coluna = 0; coluna < 8; coluna++)
        {
            int col = Random.Range(0, 7), lin = Random.Range(0, 7);
            if (Matriz[col, lin, 2].bloco)
            {
                /*faz uma checagem de teste seguindo a seguinte regra:
                 1 - seleciona aleatoriamente um determinado bloco atraves da sua linha e coluna no nivel 2(terceiro andar), e checa se ha
                 possibilidade de um caminho na sua frente, trás, direita e esquerda respectivamente, se houver possibilidade em algum desses,
                 ele chama a função FillWater juntamente com a coordenada apropriada (frente ou trás ou direita ou esquerda)*/
                try
                {
                    if (Matriz[col, lin + 1, 1].bloco && Matriz[col, lin + 1, 1].objetoAcima == TerrainCreator.Objeto.none)
                    {//instancia a agua para frente (+Z)
                        FillWater(col, lin + 1, comando.Frente);

                        break;
                    }

                    else if (Matriz[col, lin - 1, 1].bloco && Matriz[col, lin - 1, 1].objetoAcima == TerrainCreator.Objeto.none)
                    {//instancia a agua para trás (-Z)
                        FillWater(col, lin - 1, comando.Trás);
                        break;
                    }

                    else if (Matriz[col + 1, lin, 1].bloco && Matriz[col + 1, lin, 1].objetoAcima == TerrainCreator.Objeto.none)
                    {//instancia a agua para direita (+X)
                        FillWater(col + 1, lin, comando.Direita);

                        break;
                    }

                    else if (Matriz[col - 1, lin, 1].bloco && Matriz[col - 1, lin, 1].objetoAcima == TerrainCreator.Objeto.none)
                    {//instancia a agua para esquerda (-X)
                        FillWater(col - 1, lin, comando.Esquerda);
                        break;
                    }
                }

                catch { }
            }

            if (coluna == 7 && linha < 7)
            { coluna = -1; linha++; }
        }
    }

    /// <summary>
    /// preenche um determinado local com agua a partir de determinadas coordenadas.
    /// </summary>
    /// <param name="colunaInicial">Coluna inicial de instanciamento</param>
    /// <param name="linhaInicial">linha inicial de instanciamento</param>
    /// <param name="comando">coordenadas a serem seguidas</param>
    void FillWater(int colunaInicial, int linhaInicial, comando comando)
    {
        /*Segue a seguinte regra:
         1 - para cada caso de comando (frente, trás etc...), se no seu próximo bloco (Ex: se (comando == frente); então (current bloco na matriz = [2,5,6]; prox bloco na matriz = [2,6,6]) no nivel 
         abaixo não houver bloco em cima, então instanciar-se-á uma waterfall no bloco atual e diminuirá o current level -1, caso contrario, instanciar-se-á uma water e o bloco atual se torna o proximo, e assim por diante;*/
        int currentColuna = colunaInicial, currentLinha = linhaInicial, currentLevel = 1;
        switch (comando)
        {
            case EnvirommentsEngine.comando.Frente:
                while (Matriz[currentColuna, currentLinha, currentLevel].bloco &&
                    Matriz[currentColuna, currentLinha, currentLevel].objetoAcima == TerrainCreator.Objeto.none)
                {//coordenadas de onde será instanciado o proximo bloco de agua
                    Vector3 coord = new Vector3(Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.x,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.y + 1,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.z);
                    try
                    {//se o proximo bloco do nivel abaixo não houver um bloco acima dele, vai instanciar uma waterfall e o current level vai diminuir 1
                        if (Matriz[currentColuna, currentLinha + 1, currentLevel - 1].objetoAcima == TerrainCreator.Objeto.none)
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(waterFall, coord, Quaternion.Euler(0, 90, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                            currentLevel--;
                        }

                        else
                        {//senão vai instanciar uma water normal e seguir em frente (... a vida é assim mesmo, temos que seguir em  frente hueheuheue)
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, 90, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                        }
                    }
                    catch
                    {//se a checagem der erro, instancia water normal;
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, 90, 0)) as GameObject;
                        Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                    }

                    currentLinha++;
                }
                break;

            case EnvirommentsEngine.comando.Trás:
                while (Matriz[currentColuna, currentLinha, currentLevel].bloco &&
                    Matriz[currentColuna, currentLinha, currentLevel].objetoAcima == TerrainCreator.Objeto.none)
                {
                    Vector3 coord = new Vector3(Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.x,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.y + 1,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.z);
                    try
                    {
                        if (Matriz[currentColuna, currentLinha - 1, currentLevel - 1].objetoAcima == TerrainCreator.Objeto.none)
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(waterFall, coord, Quaternion.Euler(0, -90, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                            currentLevel--;
                        }

                        else
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, -90, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                        }
                    }
                    catch
                    {
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, -90, 0)) as GameObject;
                        Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                    }

                    currentLinha--;
                }
                break;

            case EnvirommentsEngine.comando.Direita:
                while (Matriz[currentColuna, currentLinha, currentLevel].bloco &&
                    Matriz[currentColuna, currentLinha, currentLevel].objetoAcima == TerrainCreator.Objeto.none)
                {
                    Vector3 coord = new Vector3(Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.x,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.y + 1,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.z);
                    try
                    {
                        if (Matriz[currentColuna + 1, currentLinha, currentLevel - 1].objetoAcima == TerrainCreator.Objeto.none)
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(waterFall, coord, Quaternion.Euler(0, 180, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                            currentLevel--;
                        }

                        else
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, 180, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                        }
                    }
                    catch
                    {
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, 180, 0)) as GameObject;
                        Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                    }

                    currentColuna++;
                }
                break;

            case EnvirommentsEngine.comando.Esquerda:
                while (Matriz[currentColuna, currentLinha, currentLevel].bloco &&
                    Matriz[currentColuna, currentLinha, currentLevel].objetoAcima == TerrainCreator.Objeto.none)
                {
                    Vector3 coord = new Vector3(Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.x,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.y + 1,
                        Matriz[currentColuna, currentLinha, currentLevel].bloco.transform.position.z);
                    try
                    {
                        if (Matriz[currentColuna - 1, currentLinha, currentLevel - 1].objetoAcima == TerrainCreator.Objeto.none)
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(waterFall, coord, Quaternion.Euler(0, 0, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                            currentLevel--;
                        }

                        else
                        {
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, 0, 0)) as GameObject;
                            Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                            Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                        }
                    }
                    catch
                    {
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima = Instantiate(water, coord, Quaternion.Euler(0, 0, 0)) as GameObject;
                        Matriz[currentColuna, currentLinha, currentLevel].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                        Matriz[currentColuna, currentLinha, currentLevel].gameObjectAcima.transform.parent = Parentesco("Agua");
                    }

                    currentColuna--;
                }
                break;

            default:
                Debug.LogError("Erro inesperado!");
                break;
        }
    }

    /// <summary>
    /// gera respectivamente, todas as pedras e todas as arvores da fase.
    /// </summary>
    void GenerateSecondaryEnviromments()
    {
        /*seleciona uma quantidade randomica de pedras a serem instanciadas, entre 0 e 4 pedras, e instanceia-as da seguinte maneira:
         1 - seleciona um determinado bloco de nivel 0 (primeiro andar) com linha e coluna randomica;
         2 - enquanto este determinado jogo de coluna e linha não retornar dizendo que não há nenhum objeto em cima dele, vai tentando subir o nivel(andar)
         3 - se chegar no ultimo nivel e ainda sim não retornar dizendo que não ha bloco em cima, o mesmo será abandonado, e o objeto não será instanciado;
         4 - se houver mais um objeto pra ser instanciado, vai tentar fazer a checagem novamente; */
        int qnt = Random.Range(0, 4);
        for (int i = 0; i < qnt; i++)
        {
            int coluna = Random.Range(0, 7), linha = Random.Range(0, 7);
            int index = 0;
            bool abandonar = false;

            while (Matriz[coluna, linha, index].objetoAcima != TerrainCreator.Objeto.none)
            { index++; if (index > 2) { abandonar = true; break; } }

            if (!abandonar)
            {
                try
                {
                    Transform posicao = Matriz[coluna, linha, index].bloco.transform;
                    Matriz[coluna, linha, index].gameObjectAcima = Instantiate(pedra, posicao.position + Vector3.up, Quaternion.Euler(0, Random.Range(0, 364), 0)) as GameObject;
                    Matriz[coluna, linha, index].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                    Matriz[coluna, linha, index].gameObjectAcima.transform.parent = Parentesco("Pedra");
                }
                catch { }
            }
        }

        //mesma regra acima, so que para arvores;
        qnt = Random.Range(0, 4);
        for (int i = 0; i < qnt; i++)
        {
            int coluna = Random.Range(0, 7), linha = Random.Range(0, 7);
            int index = 0;
            bool abandonar = false;

            while (Matriz[coluna, linha, index].objetoAcima != TerrainCreator.Objeto.none)
            { index++; if (index > 2) { abandonar = true; break; } }

            if (!abandonar)
            {
                try
                {
                    Transform posicao = Matriz[coluna, linha, index].bloco.transform;
                    Matriz[coluna, linha, index].gameObjectAcima = Instantiate(pinheiro, posicao.position + new Vector3(0, .3f, 0), Quaternion.Euler(0, Random.Range(0, 364), 0)) as GameObject;
                    Matriz[coluna, linha, index].objetoAcima = TerrainCreator.Objeto.Obstaculo;
                    Matriz[coluna, linha, index].gameObjectAcima.transform.parent = Parentesco("Arvore");
                }
                catch { }
            }
        }
    }

    /// <summary>
    /// Procura na hierarquia um Transform com base no parâmetro, caso não encontre o Transform retornado é o Transform que carrega este script
    /// </summary>
    /// <param name="TransformPai">Nome do Transform a ser procurado e retornado
    /// Os valores devem ser: Arvore, Pedra, Agua ou Outros</param>
    /// <returns></returns>
    Transform Parentesco(string TransformPai)
    {
        Transform pai;
        try { pai = GameObject.Find(TransformPai).transform; }
        catch { Debug.LogWarning("O Parentesco não foi encontado, o objeto não será aparentado neste tranform!"); pai = transform; }
        return pai;
    }
}