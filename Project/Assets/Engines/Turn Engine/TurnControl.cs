﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Turn Control System
/// </summary>
public class TurnControl : MonoBehaviour {
    //Este sistema controlará todo o turno e fará a validação dos soldados que podem mover em determinado turno
    public Slider timeLine;
    public Image _corDoSlider;

    public enum Turno { Jogador1 = 0, Jogador2 };

    static private Image corDoSlider;
    static private float TurnTime = 90;
    static public Turno turnoAtual = Turno.Jogador1;

    void SetPublicToStatic()
    { corDoSlider = _corDoSlider;}

    void Start()
    { SetPublicToStatic(); }

	void Update () {
        ContarTempo();
	}

    void ContarTempo()
    {
        TurnTime -= Time.deltaTime;

        if (TurnTime <= 0)
        { MudarTurno(); }

        timeLine.value = TurnTime;
    }

    static public void MudarTurno()
    {
        if (turnoAtual == Turno.Jogador1)
        {
            InterfaceDeMovimentacao.DestruirCaminhos();
            corDoSlider.color = new Color(0.8984375f, 1, 0, 1);
            turnoAtual = Turno.Jogador2; TurnTime = 90;
            foreach (Soldados.Guerreiro guerreiro in PlayerControl.guerreiros)
            { guerreiro.podeMover = false; }

            foreach (Soldados.Arqueiro arqueiro in PlayerControl.arqueiros)
            { arqueiro.podeMover = false; }

            foreach (Soldados.Mago mago in PlayerControl.magos)
            { mago.podeMover = false; }

            foreach (Soldados.Tenda tenda in PlayerControl.tendas)
            { tenda.podeMover = false; }
        }

        else { 
            corDoSlider.color = new Color(0,.5f,1,1);
            turnoAtual = Turno.Jogador1; TurnTime = 90;
            foreach (Soldados.Guerreiro guerreiro in PlayerControl.guerreiros)
            { guerreiro.podeMover = true; }

            foreach (Soldados.Arqueiro arqueiro in PlayerControl.arqueiros)
            { arqueiro.podeMover = true; }

            foreach (Soldados.Mago mago in PlayerControl.magos)
            { mago.podeMover = true; }

            foreach (Soldados.Tenda tenda in PlayerControl.tendas)
            { tenda.podeMover = true; }
        }

        PlayerControl.MudarCorDeObjetos();
    }

    static public void DiminuirTempo(float tempoDiminuido)
    { TurnTime -= tempoDiminuido; }
}
