﻿using System;
using System.Collections;
using UnityEngine;

public class WaterControl : MonoBehaviour {
	public Texture[] texturas = new Texture[16];
	float frametime = 0.1f;
	int index = 0;

	void Start()
	{
		InvokeRepeating ("ChangeTexture", frametime, frametime);
	}

	void ChangeTexture()
	{
		renderer.material.SetTexture ("_MainTex", texturas [index]);
		index++;

		if(index > texturas.Length-1)
		{ index = 0; }

	}

}
