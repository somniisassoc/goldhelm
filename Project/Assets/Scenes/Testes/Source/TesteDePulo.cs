﻿using UnityEngine;
using System.Collections;

public class TesteDePulo : MonoBehaviour {
    public bool _maisAlto, _podePular;
    float _posiY = 0;

	void Start () {
	
	}

	void Update () {
        if(Input.GetKeyDown(KeyCode.Return))
        { _podePular = true; }

        if(_podePular)
        { Pular(); }

        transform.position = new Vector3(transform.position.x, _posiY, transform.position.z);
	
	}

    float maxTimeDelta = 0;
    void Pular()
    {
        //função que descreve o pulo F(x) = -4X² + 4X
        if (_maisAlto)
        {
            maxTimeDelta += Time.deltaTime*2;
            if (maxTimeDelta > .5f)
            { maxTimeDelta = .5f; }
            _posiY = (-4 * Mathf.Pow(maxTimeDelta, 2)) + (4 * maxTimeDelta);

            if (maxTimeDelta >= .5f)
            { maxTimeDelta = .5f; _posiY = 1; _podePular = false; }
        }

        else
        {
            maxTimeDelta += Time.deltaTime*2;
            if (maxTimeDelta >= 1.2f)
            { maxTimeDelta = 1.2f; }
            _posiY = (-4 * Mathf.Pow(maxTimeDelta, 2)) + (4 * maxTimeDelta);

            if (maxTimeDelta >= 1.2f)
            { maxTimeDelta = 1.2f; _posiY = -1f; _podePular = false; }
        }

    }
}
