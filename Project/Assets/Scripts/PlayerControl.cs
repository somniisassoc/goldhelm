﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Controle geral de dados do jogador
/// </summary>
public class PlayerControl : MonoBehaviour
{
    /*O script funcionará da seguinte maneira: 
     1 - Ao iniciar o jogo, logo no primeiro frame vai chamar uma função para checar se há tendas em jogo, se não houver ele vai instanciá-las;
     2 - Após a confirmação de todas as tendas instanciadas, ele vai chamar uma função que fará uma checagem se o jogador clicou em alguma tenda e destruirá a primeira função;
     3 - Ao clicar em uma tenda, ele vai checar qual o tipo de tenda foi acionada, e vai ativar informações sobre o soldado dessa tenda, alem de ativar o sistema de compra;
     4 - Após a confirmação da compra de um soldado, ele vai esperar que o jogador indique onde quer instanciar o personagem;
     5 - Após saber onde será instanciado o personagem, o mesmo será instanciado;*/

    TerrainCreator.Terreno[, ,] Matriz = TerrainCreator.Blocos;
    //variaveis a serem definidas no Inspector
    public GameObject guerreiro, arqueiro, mago, tendaGuerreiro, tendaArqueiro, tendaMago;

    static public List<Soldados.Guerreiro> guerreiros = new List<Soldados.Guerreiro>();
    static public List<Soldados.Arqueiro> arqueiros = new List<Soldados.Arqueiro>();
    static public List<Soldados.Mago> magos = new List<Soldados.Mago>();
    static public List<Soldados.Tenda> tendas = new List<Soldados.Tenda>(3);

    string tempTag;
    static public bool compraAutorizada = false;
    static public int dinheiro = 2000;

    void Start()
    { InvokeRepeating("CheckTendas", 1, Time.deltaTime * 10);}

    void Update()
    {
        if (compraAutorizada && TurnControl.turnoAtual == TurnControl.Turno.Jogador1)
        { SelecionarLocalDeInstanciamento(tempTag); }

        if(TurnControl.turnoAtual != TurnControl.Turno.Jogador1)
        { compraAutorizada = false; }
    }

    /// <summary>
    /// fará a checagem ao inicio do jogo para ver se todas as tendas ja foram instanciadas, senão, vai obrigar o Player a instanciá-las
    /// </summary>
    void CheckTendas()
    {
        if (TurnControl.turnoAtual == TurnControl.Turno.Jogador1)
        {
            if (tendas.Count == 0)
            {
                Interface.AtivarInformacoes("TendaGuerreiro");
                if (Input.GetMouseButton(0))
                {
                    Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit choque = new RaycastHit();
                    if (Physics.Raycast(raio, out choque))
                    {
                        Vector3 indiceMatriz = TerrainCreator.FindBlockInMatrix(choque.transform.gameObject, true);
                        if (indiceMatriz != new Vector3())
                        { InstanciarTendas("TendaGuerreiro", indiceMatriz, tendaGuerreiro); }
                    }
                }
            }

            else if (tendas.Count == 1)
            {
                Interface.AtivarInformacoes("TendaArqueiro");
                if (Input.GetMouseButton(0))
                {
                    Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit choque = new RaycastHit();
                    if (Physics.Raycast(raio, out choque))
                    {
                        Vector3 indiceMatriz = TerrainCreator.FindBlockInMatrix(choque.transform.gameObject, true);
                        if (indiceMatriz != new Vector3())
                        { InstanciarTendas("TendaArqueiro", indiceMatriz, tendaArqueiro); }
                    }
                }
            }

            else if (tendas.Count == 2)
            {
                Interface.AtivarInformacoes("TendaMago");
                if (Input.GetMouseButton(0))
                {
                    Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
                    RaycastHit choque = new RaycastHit();
                    if (Physics.Raycast(raio, out choque))
                    {
                        Vector3 indiceMatriz = TerrainCreator.FindBlockInMatrix(choque.transform.gameObject, true);
                        if (indiceMatriz != new Vector3())
                        { InstanciarTendas("TendaMago", indiceMatriz, tendaMago); }
                    }
                }
            }

            else if (tendas.Count > 2) { InvokeRepeating("SelecionarTipoDeSoldadoPelaTenda", 1, Time.deltaTime); StartCoroutine(InterfaceDeMovimento.ChecarObjetoSelecionado()); Interface.DesativarInformacoes(); CancelInvoke("CheckTendas"); }
        }
    }

    /// <summary>
    /// Ativa o sistema para comprar soldado com base na tenda clicada
    /// </summary>
    void SelecionarTipoDeSoldadoPelaTenda()
    {
        //Ao clicar numa tenda, analisa sua tag, e com base nisso, chama o sistema de compras
        if (Input.GetMouseButtonDown(0))
        {
            Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit choque = new RaycastHit();
            if (Physics.Raycast(raio, out choque))
            {
                if (ValidarSistemaDeCompra(choque.transform.gameObject))
                {
                    Vector3 posicao = choque.transform.position;
                    if (choque.transform.gameObject.transform.CompareTag("TendaGuerreiro"))
                    { Interface.AtivarBotaoCompraVenda(posicao); tempTag = "Guerreiro"; }

                    else if (choque.transform.gameObject.transform.CompareTag("TendaArqueiro"))
                    { Interface.AtivarBotaoCompraVenda(posicao); tempTag = "Arqueiro"; }

                    else if (choque.transform.gameObject.transform.CompareTag("TendaMago"))
                    { Interface.AtivarBotaoCompraVenda(posicao); tempTag = "Mago"; }
                    InterfaceDeMovimentacao.DestruirCaminhos();
                }
            }
        }
    }

    bool ValidarSistemaDeCompra(GameObject tendaSelecionada)
    {
        int indice = 0;
        try
        {
            while (tendas[indice].objeto != tendaSelecionada)
            { indice++; }

            if (tendas[indice].objeto == tendaSelecionada)
            { return tendas[indice].podeMover; }

            else { return false; }
        }

        catch { return false; }
    }

    public void ValidarAtivacaoCompra()
    {
        Interface.AtivarCompra(tempTag);
    }

    public void ValidarAtivacaoVenda()
    { InvokeRepeating("ReceberSoldadoCandidato", 0, Time.deltaTime); }

    void ReceberSoldadoCandidato()
    {
        if (TurnControl.turnoAtual == TurnControl.Turno.Jogador1)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit choque = new RaycastHit();
                if (Physics.Raycast(raio, out choque))
                {
                    Vector3 posicao = TerrainCreator.FindGameObjectInMatrix(choque.transform.gameObject, true);
                    if (posicao != new Vector3())
                    {
                        Interface.AtivarVenda(posicao);
                        CancelInvoke("ReceberSoldadoCandidato");
                    }
                }
            }
        }

        else { CancelInvoke("ReceberSoldadoCandidato"); }
    }

    /// <summary>
    /// Seleciona o local para ser instanciado com base no local clicado
    /// </summary>
    /// <param name="tag">Tag do objeto a ser instanciado</param>
    void SelecionarLocalDeInstanciamento(string tag)
    {
        //Ao ter um local selecionado, se for valido, chama a função de instanciamento de soldados
        if (Input.GetMouseButtonDown(0))
        {
            Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit choque = new RaycastHit();
            if (Physics.Raycast(raio, out choque))
            {
                Vector3 indiceMatriz = TerrainCreator.FindBlockInMatrix(choque.transform.gameObject, true);
                if (indiceMatriz != new Vector3())
                {
                    switch (tag)
                    {
                        case "Guerreiro":
                            InstanciarSoldados("Guerreiro", indiceMatriz, guerreiro);
                            break;

                        case "Arqueiro":
                            InstanciarSoldados("Arqueiro", indiceMatriz, arqueiro);
                            break;

                        case "Mago":
                            InstanciarSoldados("Mago", indiceMatriz, mago);
                            break;

                        default:
                            Debug.LogWarning("Seleção não indentificada!");
                            tempTag = "";
                            break;
                    }
                    tempTag = "";
                }
            }
        }
    }

    /// <summary>
    /// Instancia um soldado com base na tag, objeto e posicao na matriz
    /// </summary>
    /// <param name="tag">tag do soldados</param>
    /// <param name="indiceMatriz">posicao matricial onde será instanciado o soldado</param>
    /// <param name="objeto">Objeto a ser instanciado</param>
    void InstanciarSoldados(string tag, Vector3 indiceMatriz, GameObject objeto)
    {
        Vector3 alvo = Matriz[(int)indiceMatriz.x, (int)indiceMatriz.y, (int)indiceMatriz.z].bloco.transform.position + new Vector3(0, 1.5f, 0);

        //Olha a tag e adiciona as informações na List correta
        if (Interface.ProcessarTransacao(Interface.TipoDeTransacao.compra))
        {
            switch (tag)
            {
                case "Guerreiro":
                    guerreiros.Add(new Soldados.Guerreiro());
                    guerreiros[guerreiros.Count - 1].equipe = Soldados.Equipe.Equipe1;
                    guerreiros[guerreiros.Count - 1].Objeto = Instantiate(objeto, alvo, new Quaternion(0, 0, 0, 0)) as GameObject;
                    TerrainCreator.CopyValues(TerrainCreator.Objeto.Guerreiro, guerreiros[guerreiros.Count - 1].Objeto,
                        guerreiros[guerreiros.Count - 1].energia, guerreiros[guerreiros.Count - 1].campoDeVisao,
                        guerreiros[guerreiros.Count - 1].forcaDeAtaque, guerreiros[guerreiros.Count - 1].forcaDeDefesa, indiceMatriz);
                    break;

                case "Arqueiro":
                    arqueiros.Add(new Soldados.Arqueiro());
                    arqueiros[arqueiros.Count - 1].equipe = Soldados.Equipe.Equipe1;
                    arqueiros[arqueiros.Count - 1].Objeto = Instantiate(objeto, alvo, new Quaternion(0, 0, 0, 0)) as GameObject;
                    TerrainCreator.CopyValues(TerrainCreator.Objeto.Arqueiro, arqueiros[arqueiros.Count - 1].Objeto,
                        arqueiros[arqueiros.Count - 1].energia, arqueiros[arqueiros.Count - 1].campoDeVisao,
                        arqueiros[arqueiros.Count - 1].forcaDeAtaque, arqueiros[arqueiros.Count - 1].forcaDeDefesa, indiceMatriz);
                    break;

                case "Mago":
                    magos.Add(new Soldados.Mago());
                    magos[magos.Count - 1].equipe = Soldados.Equipe.Equipe1;
                    magos[magos.Count - 1].Objeto = Instantiate(objeto, alvo, new Quaternion(0, 0, 0, 0)) as GameObject;
                    TerrainCreator.CopyValues(TerrainCreator.Objeto.Mago, magos[magos.Count - 1].Objeto,
                        magos[magos.Count - 1].energia, magos[magos.Count - 1].campoDeVisao,
                        magos[magos.Count - 1].forcaDeAtaque, magos[magos.Count - 1].forcaDeDefesa, indiceMatriz);
                    break;
            }
        }

        compraAutorizada = false;
        MudarCorDeObjetos();
    }

    /// <summary>
    /// Instancia uma tenda com base na tag, objeto e posicao na matriz
    /// </summary>
    /// <param name="tag">tag da tenda</param>
    /// <param name="indiceMatriz">posição matricial onde será instanciada a tenda</param>
    /// <param name="tenda">objeto Tenda a ser instanciado</param>
    void InstanciarTendas(string tag, Vector3 indiceMatriz, GameObject tenda)
    {
        tendas.Add(new Soldados.Tenda());
        tendas[tendas.Count - 1].podeMover = true;
        Vector3 alvo = Matriz[(int)indiceMatriz.x, (int)indiceMatriz.y, (int)indiceMatriz.z].bloco.transform.position + new Vector3(0, .5f, 0);

        switch (tag)
        {
            case "TendaGuerreiro":
                tendas[tendas.Count - 1].objeto = Instantiate(tenda, alvo, new Quaternion(0, 0, 0, 0)) as GameObject;
                TerrainCreator.CopyValues(TerrainCreator.Objeto.TendaGuerreiro, tendas[tendas.Count - 1].objeto, tendas[tendas.Count - 1].energia, indiceMatriz);
                break;

            case "TendaArqueiro":
                tendas[tendas.Count - 1].objeto = Instantiate(tenda, alvo, new Quaternion(0, 0, 0, 0)) as GameObject;
                TerrainCreator.CopyValues(TerrainCreator.Objeto.TendaArqueiro, tendas[tendas.Count - 1].objeto, tendas[tendas.Count - 1].energia, indiceMatriz);
                break;

            case "TendaMago":
                tendas[tendas.Count - 1].objeto = Instantiate(tenda, alvo, new Quaternion(0, 0, 0, 0)) as GameObject;
                TerrainCreator.CopyValues(TerrainCreator.Objeto.TendaMago, tendas[tendas.Count - 1].objeto, tendas[tendas.Count - 1].energia, indiceMatriz);
                break;
        }
    }

    /// <summary>
    /// Busca o objeto selecionado e, se ele puder mover, retorna sua posição matricial
    /// </summary>
    /// <param name="soldado">soldado para ser checado</param>
    /// <returns></returns>
    static public Vector3 ChecarAcoesDeObjeto(GameObject soldado)
    {
        bool soldadoEncontrado = false, soldadoPodeMover = false;
        GameObject objeto = new GameObject();
        Vector3 posicaoMatricial = new Vector3();

        //Buscar o objeto nas 4 listas
        foreach (Soldados.Guerreiro guerreiro in guerreiros)
        {
            if (guerreiro.Objeto == soldado)
            {
                if(guerreiro.podeMover)
                {objeto = guerreiro.Objeto; soldadoPodeMover = true;}
                soldadoEncontrado = true;
                break;
            }
        }

        if (!soldadoEncontrado)
        {
            foreach (Soldados.Arqueiro arqueiro in arqueiros)
            {
                if (arqueiro.Objeto == soldado)
                {
                    if(arqueiro.podeMover)
                    {objeto = arqueiro.Objeto; soldadoPodeMover = true;}
                    soldadoEncontrado = true;
                    break;
                }
            }
        }

        if (!soldadoEncontrado)
        {
            foreach (Soldados.Mago mago in magos)
            {
                if (mago.Objeto == soldado)
                {
                    if(mago.podeMover)
                    {objeto = mago.Objeto; soldadoPodeMover = true;}
                    soldadoEncontrado = true;
                    break;
                }
            }
        }

        if (!soldadoEncontrado)
        {
            foreach (Soldados.Tenda tenda in tendas)
            {
                if (tenda.objeto == soldado)
                {
                    if(tenda.podeMover)
                    {objeto = tenda.objeto; soldadoPodeMover = true;}
                    soldadoEncontrado = true;
                    break;
                }
            }
        }

        if (soldadoEncontrado)
        {
            if (soldadoPodeMover)
            {
                Debug.LogWarning("O Objeto foi identificado, e tem permissão para mover!");
                posicaoMatricial = TerrainCreator.FindGameObjectInMatrix(objeto, false);
            }

            else { Debug.LogWarning("O Objeto foi identificado, porém não tem permissão para mover!"); }
        }

        else { Debug.LogWarning("O Objeto não foi identificado!"); }

        return posicaoMatricial;
    }

    /// <summary>
    /// Remove a ação de determinado objeto durante o atual turno
    /// </summary>
    /// <param name="soldado">soldado que terá suas ações removidas</param>
    static public void RemoverAcoesDeObjeto(GameObject soldado)
    {
        bool soldadoEncontrado = false;

        foreach (Soldados.Guerreiro guerreiro in guerreiros)
        {
            if (guerreiro.Objeto == soldado)
            {
                guerreiro.podeMover = false;
                soldadoEncontrado = true;
            }
        }

        if (!soldadoEncontrado)
        {
            foreach (Soldados.Arqueiro arqueiro in arqueiros)
            {
                if (arqueiro.Objeto == soldado)
                {
                    arqueiro.podeMover = false;
                    soldadoEncontrado = true;
                    break;
                }
            }
        }

        if (!soldadoEncontrado)
        {
            foreach (Soldados.Mago mago in magos)
            {
                if (mago.Objeto == soldado)
                {
                    mago.podeMover = false;
                    soldadoEncontrado = true;
                    break;
                }
            }
        }

        if (!soldadoEncontrado)
        {
            foreach (Soldados.Tenda tenda in tendas)
            {
                if (tenda.objeto == soldado)
                {
                    tenda.podeMover = false;
                    soldadoEncontrado = true;
                    break;
                }
            }
        }

        else { Debug.LogWarning("O Objeto não foi identificado!"); }
        MudarCorDeObjetos();
    }

    /// <summary>
    /// Descobre o tipo de soldado, a partir de um GameObject e retorna sua tag
    /// </summary>
    /// <param name="soldado">Soldado para ser descoberto seu tipo</param>
    /// <returns></returns>
    static public string DescobrirTipoDeSoldado(GameObject soldado)
    {
        string tag = "";
        bool soldadoEncontrado = false;

        while (!soldadoEncontrado)
        {
            foreach (Soldados.Guerreiro guerreiro in guerreiros)
            {
                if (soldado == guerreiro.Objeto)
                {
                    tag = "Guerreiro";
                    soldadoEncontrado = true;
                    break;
                }
            }

            foreach (Soldados.Arqueiro arqueiro in arqueiros)
            {
                if (soldado == arqueiro.Objeto)
                {
                    tag = "Arqueiro";
                    soldadoEncontrado = true;
                    break;
                }
            }

            foreach (Soldados.Mago mago in magos)
            {
                if (soldado == mago.Objeto)
                {
                    tag = "Mago";
                    soldadoEncontrado = true;
                    break;
                }
            }
            break;
        }

        return tag;
    }

    static public void DestruirObjeto(Vector3 posicaoMatricial)
    {
        int indice = 0;
        switch (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima)
        {
            case TerrainCreator.Objeto.Guerreiro:
                while (guerreiros[indice].Objeto != TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima)
                { indice++; }
                guerreiros.RemoveAt(indice);
                guerreiros.Sort();
                break;

            case TerrainCreator.Objeto.Arqueiro:
                while (arqueiros[indice].Objeto != TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima)
                { indice++; }
                arqueiros.RemoveAt(indice);
                arqueiros.Sort();
                break;

            case TerrainCreator.Objeto.Mago:
                while (magos[indice].Objeto != TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima)
                { indice++; }
                magos.RemoveAt(indice);
                magos.Sort();
                break;

            case TerrainCreator.Objeto.TendaGuerreiro:
                while (tendas[indice].objeto != TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima)
                { indice++; }
                tendas.RemoveAt(indice);
                tendas.Sort();
                break;

            case TerrainCreator.Objeto.TendaArqueiro:
                while (tendas[indice].objeto != TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima)
                { indice++; }
                tendas.RemoveAt(indice);
                tendas.Sort();
                break;

            case TerrainCreator.Objeto.TendaMago:
                while (tendas[indice].objeto != TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima)
                { indice++; }
                tendas.RemoveAt(indice);
                tendas.Sort();
                break;
        }

        Destroy(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].gameObjectAcima);
        TerrainCreator.DeleteValues(posicaoMatricial);
    }

    static public void MudarCorDeObjetos()
    {
        foreach(Soldados.Guerreiro guerreiro in guerreiros )
        {
            if(!guerreiro.podeMover)
            { guerreiro.Objeto.renderer.material.color = Color.grey; }

            else { guerreiro.Objeto.renderer.material.color = Color.white; }
        }

        foreach(Soldados.Arqueiro arqueiro in arqueiros )
        {
            if(!arqueiro.podeMover)
            { arqueiro.Objeto.renderer.material.color = Color.grey; }

            else { arqueiro.Objeto.renderer.material.color = Color.yellow; }
        }

        foreach(Soldados.Mago mago in magos)
        {
            if(!mago.podeMover)
            { mago.Objeto.renderer.material.color = Color.grey; }

            else { mago.Objeto.renderer.material.color = Color.magenta; }
        }
    }
}
