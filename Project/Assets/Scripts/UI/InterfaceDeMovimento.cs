﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InterfaceDeMovimento : MonoBehaviour {

    public GameObject[] camposDeSelecao;
    public GameObject _botoes;
    static public GameObject botoes;
    static GameObject objetoSelecionado;
    static Vector3 posicaoDoObjetoSelecionado;
    static private List<GameObject> camposInstanciados = new List<GameObject>();
    static private List<TerrainCreator.Terreno> terrenos = new List<TerrainCreator.Terreno>();
    List<Vector3> waysF = new List<Vector3>(), waysT = new List<Vector3>(), waysD = new List<Vector3>(), waysE = new List<Vector3>();
    List<List<Vector3>> ways = new List<List<Vector3>>();
    Vector3 _posicaoMatricialAtualDoSoldado;
    Vector3 _posicaoMatricialAlvoDoSoldado;

    void Start()
    {  SetPublicToStatic(); }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        { TurnControl.MudarTurno(); }
    }


    void SetPublicToStatic()
    { botoes = _botoes; }

    static public IEnumerator ChecarObjetoSelecionado()
    {
        Vector3 posicao = new Vector3();
        while (Application.isPlaying)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit choque = new RaycastHit();
                if (Physics.Raycast(raio, out choque))
                {
                    posicao = PlayerControl.ChecarAcoesDeObjeto(choque.transform.gameObject);
                    if (posicao != new Vector3())
                    {
                        objetoSelecionado = choque.transform.gameObject;
                        posicaoDoObjetoSelecionado = posicao;
                        Vector3 posicaoGlobal = TerrainCreator.Blocos[(int)posicao.x, (int)posicao.y, (int)posicao.z].gameObjectAcima.transform.position + new Vector3(0, 1.5f, 0);
                        botoes.SetActive(true);
                        botoes.transform.position = posicaoGlobal;
                    }
                }
            }
            yield return null;
        }
    }

    public void IniciarSistemaDeMovimentacao()
    {
        //Vai Chamar a função que inicia o sistema
        DesativarBotoes();
        DestruirCaminhos();
        //StopCoroutine("ChecarObjetoSelecionado");
        DescobrirPassosEPosicaoMatricial();
    }

    void DescobrirPassosEPosicaoMatricial()
    {
        //A partir do objeto que foi selecionado anteriormente, vai descobrir a quantidade de passos que o soldado pode dar e vai chamar a função que desenha os caminhos
        string tagDoSoldado = PlayerControl.DescobrirTipoDeSoldado(objetoSelecionado);

        switch (tagDoSoldado)
        {
            case "Guerreiro":
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                DesenharCaminhos(posicaoDoObjetoSelecionado, guerreiro.campoDeMovimento);
                break;

            case "Arqueiro":
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                DesenharCaminhos(posicaoDoObjetoSelecionado, arqueiro.campoDeMovimento);
                break;

            case "Mago":
                Soldados.Mago mago = new Soldados.Mago();
                DesenharCaminhos(posicaoDoObjetoSelecionado, mago.campoDeMovimento);
                break;
        }
    }

    /// <summary>
    /// Desenha os possíveis caminhos para a movimentação do objeto
    /// </summary>
    /// <param name="posicaoMatricial">Posição matricial do Objeto</param>
    /// <param name="campoDeMovimento">Quantidade de passos que o objeto pode dar</param>
    void DesenharCaminhos(Vector3 posicaoMatricial, int campoDeMovimento)
    {
        /*Segue a seguinte regra:
         * 1 - A List camposInstanciados vai armazenar todos os game Objects que são instanciados para mostrar ao jogador onde ele pode mover seu soldado selecionado
         * 2 - Para cada posição coordenada (frente, tras, direita, esquerda), vai fazer uma checagem com o intuito de obter o bloco mais alto, e nesse bloco que obter, fará a seguinte checagem: 
         *      A) Se a diferença da altura entre o atual bloco e o bloco candidato for maior que 1, então esse terá um passo inválido e instanciará um campo de seleção vermelho;
         *      B) Se, no bloco candidato, houver algum obstáculo nele, então o mesmo será invalido, e instanciará um campo de seleção vermelho no mesmo;
         *      C) Caso as petições acima não sejam atendidas, o mesmo será validado, e instanciará um campo de seleção azul para mostrar que é possível um caminho no mesmo;
         *      D) Todos os campos instanciados são armazenados na List CamposInstanciados;
         * 
         * 3 - No bloco onde está o soldado selecionado, será instanciado um campo de seleção amarelo;
         */

        GameObject campo;
        float SavePosiZ = posicaoMatricial.z;
        campo = Instantiate(camposDeSelecao[2], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
        camposInstanciados.Add(campo);

        int passosDados;

        //FRENTE ----------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;

            try
            {
                //Enquanto o bloco candidato numa determinada altura nao estiver com nada acima dele, vai tentar aumentar a altura, até antender esta requisição
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    //Se em determinado bloco houver outro objeto acima senão terreno, o passo será invalidado no mesmo momento e então o loop while será quebrado
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                //checagem para garantir que nao busque níveis de blocos que não existem
                if (currentNivel > 2)
                { currentNivel = 2; }

                //Se a diferença da altura do bloco atual e a altura do bloco candidato for maior que 1, então o passo será invalido
                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                //Caso o passo seja  válido instanciará o campo de seleção azul e bla bla bla
                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    waysF.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].posicaoMatricial);
                    posicaoMatricial.z = currentNivel;
                }

                //Se não for válido, o campo instanciado será o vermelho
                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    //terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    //waysF.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].posicaoMatricial);
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //TRÁS ----------------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                if (currentNivel > 2)
                { currentNivel = 2; }

                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    waysT.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].posicaoMatricial);
                    posicaoMatricial.z = currentNivel;
                }

                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    //terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    //waysT.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].posicaoMatricial);
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //DIREITA -----------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                if (currentNivel > 2)
                { currentNivel = 2; }

                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    waysD.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].posicaoMatricial);
                    posicaoMatricial.z = currentNivel;
                }

                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    //terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    //waysD.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].posicaoMatricial);
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //ESQUERDA ------------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                if (currentNivel > 2)
                { currentNivel = 2; }

                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    waysE.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].posicaoMatricial);
                    posicaoMatricial.z = currentNivel;
                }

                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    //terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    //waysE.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].posicaoMatricial);
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;
        ways.Add(waysF);
        ways.Add(waysT);
        ways.Add(waysD);
        ways.Add(waysE);

        //Após desenhar os caminhos, é necessário esperar o bloco onde o Jogador quer que o soldado vá
        StartCoroutine(EsperarPosicao());
    }

    IEnumerator EsperarPosicao()
    {
        //TODO: Não esquecer de diminuir o tempo do turno por quantidade de passos;
        //TODO: Não esquecer de desmacar o podeMover do objeto após ele ter se movimentado;

        while (Application.isPlaying)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit choque = new RaycastHit();
                if (Physics.Raycast(raio, out choque))
                {
                    EnvirommentsEngine.comando direcao;
                    _posicaoMatricialAlvoDoSoldado = FindBlockAndDirection(choque.transform.gameObject, out direcao);

                    //Se a posição selecionada for válida, o novo alvo do Soldado será essa posição selecionada pelo jogador;
                    if (_posicaoMatricialAlvoDoSoldado != new Vector3())
                    {
                        Vector3 alvo = TerrainCreator.Blocos[(int)_posicaoMatricialAlvoDoSoldado.x, (int)_posicaoMatricialAlvoDoSoldado.y, (int)_posicaoMatricialAlvoDoSoldado.z].bloco.transform.position + new Vector3(0, 1.5f, 0);
                        TerrainCreator.TransferValues(posicaoDoObjetoSelecionado, _posicaoMatricialAlvoDoSoldado);
                        _posicaoMatricialAtualDoSoldado = posicaoDoObjetoSelecionado;
                        StartCoroutine(MoverSoldado(objetoSelecionado, GenerateWayPointSystem(ways, direcao)));
                        StopCoroutine(EsperarPosicao());
                        break;
                    }

                    //Senão vai destruir os caminhos instanciados e cancelar a espera da seleção do bloco do jogador;
                    else { DestruirCaminhos(); DestruirWayPoints(); StopCoroutine(EsperarPosicao()); break; }
                }
            }
            yield return null;
        }
    }

    IEnumerator MoverSoldado(GameObject soldado, Vector3[] wayPoints)
    {
        int index = 0;
        TurnControl.DiminuirTempo(7);
        while (Application.isPlaying)
        {
            if (!IsSuficientNear(soldado.transform.position, TerrainCreator.Blocos[(int)wayPoints[index].x, (int)wayPoints[index].y, (int)wayPoints[index].z].bloco.transform.position + new Vector3(0, 1.5f, 0)))
            { soldado.transform.position = Vector3.Slerp(soldado.transform.position, TerrainCreator.Blocos[(int)wayPoints[index].x, (int)wayPoints[index].y, (int)wayPoints[index].z].bloco.transform.position + new Vector3(0, 1.5f, 0), 10 * Time.deltaTime); }

            else if (index < wayPoints.Length-1)
            { index++; TurnControl.DiminuirTempo(7); }

            else { 
                StopCoroutine(MoverSoldado(soldado, wayPoints)); 
                PlayerControl.RemoverAcoesDeObjeto(soldado); 
                DestruirCaminhos(); 
                DestruirWayPoints(); 
                break;
            }
            yield return null;
        }


    }

    Vector3[] GenerateWayPointSystem(List<List<Vector3>> carretelDeWays, EnvirommentsEngine.comando direcao)
    {
        List<Vector3> pointsToWay = new List<Vector3>();
        List<Vector3> wayPoints = new List<Vector3>();
        switch (direcao)
        {
            case EnvirommentsEngine.comando.Frente:
                pointsToWay = carretelDeWays[0];
                break;

            case EnvirommentsEngine.comando.Trás:
                pointsToWay = carretelDeWays[1];
                break;

            case EnvirommentsEngine.comando.Direita:
                pointsToWay = carretelDeWays[2];
                break;

            case EnvirommentsEngine.comando.Esquerda:
                pointsToWay = carretelDeWays[3];
                break;
        }

        foreach(Vector3 way in pointsToWay)
        {
            if(way == _posicaoMatricialAlvoDoSoldado)
            { wayPoints.Add(way); break; }

            else { wayPoints.Add(way); }
        }

        return wayPoints.ToArray();
    }

    /// <summary>
    /// Descobre a posição matricial do bloco selecionado e a direção em que o soldado deve se mover
    /// </summary>
    /// <param name="objeto">Bloco selecionado</param>
    /// <param name="direcao">Saida da direção selecionada</param>
    /// <returns>Posição matricial do alvo</returns>
    Vector3 FindBlockAndDirection(GameObject objeto, out EnvirommentsEngine.comando direcao)
    {
        int indice = 0;
        bool blocoEncontrado = true;
        Vector3 posicao = new Vector3();
        EnvirommentsEngine.comando comando = new EnvirommentsEngine.comando();

        //Busca nas possíveis posições para movimento que foram salvas, o bloco selecionado;
        while (objeto != terrenos[indice].bloco)
        {
            indice++;

            //Checagem para nao haver um loop infinito caso nao seja encontrado o determinado bloco
            if (indice > terrenos.Count - 1)
            { blocoEncontrado = false; break; }
        }

        //Se o bloco for encontrado, será feito um cálculo de diferença para saber qual será direção pelo qual o soldado moverá
        if (blocoEncontrado)
        {
            posicao = TerrainCreator.FindBlockInMatrix(terrenos[indice].bloco, false);
            if (posicao != new Vector3())
            {
                int difX = (int)(posicaoDoObjetoSelecionado.x - posicao.x);
                if (difX > 0)
                { comando = EnvirommentsEngine.comando.Esquerda; }

                else if (difX < 0)
                { comando = EnvirommentsEngine.comando.Direita; }

                else
                {
                    int difY = (int)(posicaoDoObjetoSelecionado.y - posicao.y);
                    if (difY < 0)
                    { comando = EnvirommentsEngine.comando.Frente; }

                    else if (difY > 0)
                    { comando = EnvirommentsEngine.comando.Trás; }
                }
            }
        }
        direcao = comando;
        return posicao;
    }

    static private bool IsSuficientNear(Vector3 posicaoAtual, Vector3 posicaoAlvo)
    {
        if (Vector3.Distance(posicaoAtual, posicaoAlvo) < .1f)
        { return true; }
        else { return false; }
    }

    static public void DesativarBotoes()
    { botoes.SetActive(false); }

    static public void DestruirCaminhos()
    {
        foreach (GameObject objeto in camposInstanciados)
        { Destroy(objeto); }
        camposInstanciados.Clear();

        terrenos.Clear();
        print("Valores Destruidos");
    }

    /// <summary>
    /// Destroi todos os waypoints referentes a animação de movimentação
    /// </summary>
    void DestruirWayPoints()
    {
        waysF.Clear();
        waysT.Clear();
        waysD.Clear();
        waysE.Clear();
        ways.Clear();
    }
}
