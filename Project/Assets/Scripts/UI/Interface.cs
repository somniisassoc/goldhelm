﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// sistema de interface da parte do jogo
/// </summary>
public class Interface : MonoBehaviour {
    public GameObject _painelDeInformação;
    public Slider _energia, _forca, _defesa;
    public Text _nome, _info, _preco, _dinheiro;
    public GameObject _painelCusto, _painelDinheiro, _painelConfirmacao;
    public GameObject _compraBotao;
    public Button _comprarBotao;

    static GameObject painel;         
    static Slider forcaDeEnergia, EnergiaDeForca, energiaDeDefesa;
    static Text textoDeNome, textoDeInfo, textoPreco, textoDinheiro;
    static GameObject painelCusto, painelDinheiro, painelConfirmacao;
    static GameObject compraBotao;
    static Button comprarBotao;

    static private int custo = 0, tipoDeOperacao = 0; /*Tipo de operação = 0 [compra], 1 [venda]*/
    static private Vector3 tempPosicao = new Vector3();

    public enum TipoDeTransacao { compra = 0, venda };

    void SetPublicToStatics()
    {
        painel = _painelDeInformação;
        forcaDeEnergia = _energia;
        EnergiaDeForca = _forca;
        energiaDeDefesa = _defesa;
        textoDeNome = _nome;
        textoDeInfo = _info;
        textoPreco = _preco;
        textoDinheiro = _dinheiro;
        painelCusto = _painelCusto;
        painelDinheiro = _painelDinheiro;
        painelConfirmacao = _painelConfirmacao;
        compraBotao = _compraBotao;
        comprarBotao = _comprarBotao;
    }
    
	void Start () {
        SetPublicToStatics();
        
	}

    void Update()
    { Rotate(); }

    void QuitFase(bool finalizar)
    { Application.LoadLevel(1); }

    public void PauseGame(bool pausar)
    {
        if (pausar)
        { Time.timeScale = 0; }
        else
        { Time.timeScale = 1; }
    }

    static public void AtivarBotaoCompraVenda(Vector3 posicao)
    {
        compraBotao.transform.position = posicao + new Vector3(0,2.5f,0);
        compraBotao.SetActive(true);
    }

    static public void DesativarBotaoCompraVenda()
    {
        compraBotao.SetActive(false);
    }

    /// <summary>
    /// Mostra informações dos soldados em geral
    /// </summary>
    /// <param name="tag">Tag do soldado</param>
    /// <param name="index">Indice do soldado no array</param>
    public static void AtivarInformacoes(string tag, int index)
    {
        DesativarInformacoes();
        switch (tag)
        {
            case "Guerreiro":
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                forcaDeEnergia.maxValue = guerreiro.energia;               
                EnergiaDeForca.maxValue = guerreiro.forcaDeAtaque;
                energiaDeDefesa.maxValue = guerreiro.forcaDeDefesa;

                forcaDeEnergia.value = PlayerControl.guerreiros[index].energia;
                EnergiaDeForca.value = PlayerControl.guerreiros[index].forcaDeAtaque;
                energiaDeDefesa.value = PlayerControl.guerreiros[index].forcaDeDefesa;
                textoDeNome.text = tag;
                textoDeInfo.text = guerreiro.definicao;
                break;

            case "Arqueiro":
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                forcaDeEnergia.maxValue = arqueiro.energia;
                EnergiaDeForca.maxValue = arqueiro.forcaDeAtaque;
                energiaDeDefesa.maxValue = arqueiro.forcaDeDefesa;

                forcaDeEnergia.value = PlayerControl.arqueiros[index].energia;
                EnergiaDeForca.value = PlayerControl.arqueiros[index].forcaDeAtaque;
                energiaDeDefesa.value = PlayerControl.arqueiros[index].forcaDeDefesa;
                textoDeNome.text = tag;
                textoDeInfo.text = arqueiro.definicao;
                break;

            case "Mago":
                Soldados.Mago mago = new Soldados.Mago();
                forcaDeEnergia.maxValue = mago.energia;
                EnergiaDeForca.maxValue = mago.forcaDeAtaque;
                energiaDeDefesa.maxValue = mago.forcaDeDefesa;

                forcaDeEnergia.value = PlayerControl.magos[index].energia;
                EnergiaDeForca.value = PlayerControl.magos[index].forcaDeAtaque;
                energiaDeDefesa.value = PlayerControl.magos[index].forcaDeDefesa;
                textoDeNome.text = tag;
                textoDeInfo.text = mago.definicao;
                break;

            case "TendaGuerreiro":
                Soldados.Tenda tendaGuerreiro = new Soldados.Tenda();
                forcaDeEnergia.maxValue = tendaGuerreiro.energia;

                forcaDeEnergia.value = PlayerControl.tendas[index].energia;
                textoDeNome.text = tag;
                textoDeInfo.text = tendaGuerreiro.definicao;
                break;

            case "TendaArqueiro":
                Soldados.Tenda tendaArqueiro = new Soldados.Tenda();
                forcaDeEnergia.maxValue = tendaArqueiro.energia;

                forcaDeEnergia.value = PlayerControl.tendas[index].energia;
                textoDeNome.text = tag;
                textoDeInfo.text = tendaArqueiro.definicao;
                break;

            case "TendaMago":
                Soldados.Tenda tendaMago = new Soldados.Tenda();
                forcaDeEnergia.maxValue = tendaMago.energia;

                forcaDeEnergia.value = PlayerControl.tendas[index].energia;
                textoDeNome.text = tag;
                textoDeInfo.text = tendaMago.definicao;
                break;

            default:
                Debug.LogWarning("Tag não reconhecida!");
                break;

        }

        painel.SetActive(true);
    }
    /// <summary>
    /// Mostra informação de um determinado objeto com base na sua posição matricial
    /// </summary>
    /// <param name="posicaoMatricial">posição do objeto na matriz</param>
    public static void AtivarInformacoes(Vector3 posicaoMatricial)
    {
        switch (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima)
        {
            case TerrainCreator.Objeto.Guerreiro:
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                forcaDeEnergia.maxValue = guerreiro.energia;               
                EnergiaDeForca.maxValue = guerreiro.forcaDeAtaque;
                energiaDeDefesa.maxValue = guerreiro.forcaDeDefesa;

                forcaDeEnergia.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto;
                EnergiaDeForca.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].forcaDeAtaque;
                energiaDeDefesa.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].forcaDeDefesa;
                textoDeNome.text = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima.ToString();
                textoDeInfo.text = guerreiro.definicao;
                break;

            case TerrainCreator.Objeto.Arqueiro:
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                forcaDeEnergia.maxValue = arqueiro.energia;
                EnergiaDeForca.maxValue = arqueiro.forcaDeAtaque;
                energiaDeDefesa.maxValue = arqueiro.forcaDeDefesa;

                forcaDeEnergia.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto;
                EnergiaDeForca.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].forcaDeAtaque;
                energiaDeDefesa.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].forcaDeDefesa;
                textoDeNome.text = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima.ToString();
                textoDeInfo.text = arqueiro.definicao;
                break;

            case TerrainCreator.Objeto.Mago:
                Soldados.Mago mago = new Soldados.Mago();
                forcaDeEnergia.maxValue = mago.energia;
                EnergiaDeForca.maxValue = mago.forcaDeAtaque;
                energiaDeDefesa.maxValue = mago.forcaDeDefesa;

                forcaDeEnergia.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto;
                EnergiaDeForca.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].forcaDeAtaque;
                energiaDeDefesa.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].forcaDeDefesa;
                textoDeNome.text = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima.ToString();
                textoDeInfo.text = mago.definicao;
                break;

            case TerrainCreator.Objeto.TendaGuerreiro:
                Soldados.Tenda tendaGuerreiro = new Soldados.Tenda();
                forcaDeEnergia.maxValue = tendaGuerreiro.energia;

                forcaDeEnergia.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto;
                textoDeNome.text = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima.ToString();
                textoDeInfo.text = tendaGuerreiro.definicao;
                break;

            case TerrainCreator.Objeto.TendaArqueiro:
                Soldados.Tenda tendaArqueiro = new Soldados.Tenda();
                forcaDeEnergia.maxValue = tendaArqueiro.energia;

                forcaDeEnergia.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto;
                textoDeNome.text = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima.ToString();
                textoDeInfo.text = tendaArqueiro.definicao;
                break;

            case TerrainCreator.Objeto.TendaMago:
                Soldados.Tenda tendaMago = new Soldados.Tenda();
                forcaDeEnergia.maxValue = tendaMago.energia;

                forcaDeEnergia.value = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto;
                textoDeNome.text = TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima.ToString();
                textoDeInfo.text = tendaMago.definicao;
                break;

            default:
                Debug.LogWarning("Objeto não reconhecido!");
                break;
        }

        painel.SetActive(true);
    }
    /// <summary>
    /// Mostra informacoes dos soldados em geral (voltada a compras)
    /// </summary>
    /// <param name="tag">tag do soldado</param>
    public static void AtivarInformacoes(string tag)
    {
        DesativarInformacoes();
        switch (tag)
        {
            case "Guerreiro":
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                forcaDeEnergia.maxValue = forcaDeEnergia.value = guerreiro.energia;
                EnergiaDeForca.maxValue = EnergiaDeForca.value = guerreiro.forcaDeAtaque;
                energiaDeDefesa.maxValue = energiaDeDefesa.value = guerreiro.forcaDeDefesa;

                textoDeNome.text = tag;
                textoDeInfo.text = guerreiro.definicao;
                break;

            case "Arqueiro":
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                forcaDeEnergia.maxValue = forcaDeEnergia.value = arqueiro.energia;
                EnergiaDeForca.maxValue = EnergiaDeForca.value = arqueiro.forcaDeAtaque;
                energiaDeDefesa.maxValue = energiaDeDefesa.value = arqueiro.forcaDeDefesa;

                textoDeNome.text = tag;
                textoDeInfo.text = arqueiro.definicao;
                break;

            case "Mago":
                Soldados.Mago mago = new Soldados.Mago();
                forcaDeEnergia.maxValue = forcaDeEnergia.value = mago.energia;
                EnergiaDeForca.maxValue = EnergiaDeForca.value = mago.forcaDeAtaque;
                energiaDeDefesa.maxValue = energiaDeDefesa.value = mago.forcaDeDefesa;

                textoDeNome.text = tag;
                textoDeInfo.text = mago.definicao;
                break;

            case "TendaGuerreiro":
                Soldados.Tenda tendaGuerreiro = new Soldados.Tenda();
                forcaDeEnergia.maxValue = forcaDeEnergia.value = tendaGuerreiro.energia;
                EnergiaDeForca.maxValue = EnergiaDeForca.value = 0;
                energiaDeDefesa.maxValue = energiaDeDefesa.value = 0;

                textoDeNome.text = tag;
                textoDeInfo.text = tendaGuerreiro.definicao;
                break;

            case "TendaArqueiro":
                Soldados.Tenda tendaArqueiro = new Soldados.Tenda();
                forcaDeEnergia.maxValue = forcaDeEnergia.value = tendaArqueiro.energia;
                EnergiaDeForca.maxValue = EnergiaDeForca.value = 0;
                energiaDeDefesa.maxValue = energiaDeDefesa.value = 0;

                textoDeNome.text = tag;
                textoDeInfo.text = tendaArqueiro.definicao;
                break;

            case "TendaMago":
                Soldados.Tenda tendaMago = new Soldados.Tenda();
                forcaDeEnergia.maxValue = forcaDeEnergia.value = tendaMago.energia;
                EnergiaDeForca.maxValue = EnergiaDeForca.value = 0;
                energiaDeDefesa.maxValue = energiaDeDefesa.value = 0;

                textoDeNome.text = tag;
                textoDeInfo.text = tendaMago.definicao;
                break;
        }

        painel.SetActive(true);
    }
    /// <summary>
    /// Mostra na tela um display com informações sobre a tenda
    /// </summary>
    /// <param name="TendaTag">Tipo de tenda</param>
    /// <param name="indexTenda">Indice da tenda no array</param>
    public static void AtivarInformacoes(int indexTenda, string TendaTag)
    {
        DesativarInformacoes();
        Soldados.Tenda tenda = new Soldados.Tenda();

        forcaDeEnergia.maxValue = tenda.energia;
        forcaDeEnergia.value = PlayerControl.tendas[indexTenda].energia;
        textoDeNome.text = PlayerControl.tendas[indexTenda].objeto.ToString();
        textoDeInfo.text = PlayerControl.tendas[indexTenda].definicao;
        painel.SetActive(true);
    }

    /// <summary>
    /// Desativa o display de informações
    /// </summary>
    public static void DesativarInformacoes()
    {
        painel.SetActive(false);
        forcaDeEnergia.value = 0;
        EnergiaDeForca.value = 0;
        energiaDeDefesa.value = 0;
        textoDeNome.text = "";
        textoDeInfo.text = "";
    }

    /// <summary>
    /// Ativa um display com informações do soldado candidato a ser comprado
    /// </summary>
    /// <param name="tag">tag do Soldado</param>
    static public void AtivarCompra(string tag)
    {
        tipoDeOperacao = 0;
        DesativarBotaoCompraVenda();
        InterfaceDeMovimento.DesativarBotoes();
        InterfaceDeMovimento.DestruirCaminhos();
        switch (tag)
        {
            case "Guerreiro":
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                custo = guerreiro.preco;
                textoPreco.text = custo.ToString();
                break;

            case "Arqueiro":
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                custo = arqueiro.preco;
                textoPreco.text = custo.ToString();
                break;

            case "Mago":
                Soldados.Mago mago = new Soldados.Mago();
                custo = mago.preco;
                textoPreco.text = custo.ToString();
                break;
        }

        painelConfirmacao.SetActive(true);
        painelCusto.SetActive(true);
        textoDinheiro.text = PlayerControl.dinheiro.ToString();
        painelDinheiro.SetActive(true);

        AtivarInformacoes(tag);
    }

    static public void AtivarVenda(Vector3 posicaoMatricial)
    {
        tipoDeOperacao = 1;
        DesativarBotaoCompraVenda();
        switch (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].objetoAcima)
        {
            case TerrainCreator.Objeto.Guerreiro:
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                custo = (int)(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto / guerreiro.energia) * guerreiro.preco;
                textoPreco.text = custo.ToString();
                break;

            case TerrainCreator.Objeto.Arqueiro:
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                custo = (int)(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto / arqueiro.energia) * arqueiro.preco;
                textoPreco.text = custo.ToString();
                break;

            case TerrainCreator.Objeto.Mago:
                Soldados.Mago mago = new Soldados.Mago();
                custo = (int)(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].energiaDoObjeto / mago.energia) * mago.preco;
                textoPreco.text = custo.ToString();
                break;
        }

        painelConfirmacao.SetActive(true);
        painelCusto.SetActive(true);
        textoDinheiro.text = PlayerControl.dinheiro.ToString();
        painelDinheiro.SetActive(true);

        AtivarInformacoes(posicaoMatricial);
        tempPosicao = posicaoMatricial;
    }

    /// <summary>
    /// Confirma a ação tomada (atualmente so confirma a compra validando-a e descontando do jogador o valor) e desativa os displays
    /// </summary>
    public void ConfirmarAcao()
    {
        switch (tipoDeOperacao)
        {
            case 0:
                PlayerControl.compraAutorizada = true;
                break;

            case 1:
                PlayerControl.dinheiro += custo;
                PlayerControl.DestruirObjeto(tempPosicao);
                break;
        }


        painelConfirmacao.SetActive(false);
        painelCusto.SetActive(false);
        painelDinheiro.SetActive(false);
        DesativarInformacoes();
    }

    static public bool ProcessarTransacao(TipoDeTransacao tipo)
    {
        bool transacaoRealizada = false;
        switch (tipo)
        {
            case TipoDeTransacao.compra:
                if (PlayerControl.dinheiro >= custo)
                {
                    PlayerControl.dinheiro -= custo;
                    PlayerControl.compraAutorizada = true;
                    custo = 0;
                    transacaoRealizada = true;
                }
                break;
        }

        return transacaoRealizada;
    }

    /// <summary>
    /// Cancela a ação a ser tomada e desativa os displays
    /// </summary>
    public void CancelarAcao()
    {
        painelConfirmacao.SetActive(false);
        painelCusto.SetActive(false);
        painelDinheiro.SetActive(false);
        custo = 0;
        DesativarInformacoes();
    }

    void Rotate()
    {
        if (Input.GetMouseButton(0))
        {
            Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit choque = new RaycastHit();
            if (!Physics.Raycast(raio, out choque))
            {
                DesativarBotaoCompraVenda();
                InterfaceDeMovimento.DesativarBotoes();
                GameObject camera = Camera.main.gameObject;
                if (Input.mousePosition.y < Screen.height / 2)
                { camera.transform.RotateAround(new Vector3(10, 0, 10), Vector3.up, Input.GetAxis("Mouse X") * Time.deltaTime * 50); }
                else { camera.transform.RotateAround(new Vector3(10, 0, 10), Vector3.up, -Input.GetAxis("Mouse X") * Time.deltaTime * 50); }
            }
        }
    }
}