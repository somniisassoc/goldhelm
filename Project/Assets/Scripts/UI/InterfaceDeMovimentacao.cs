﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Interface de movimentação
/// </summary>
public class InterfaceDeMovimentacao : MonoBehaviour
{
    public GameObject[] camposDeSelecao;
    public GameObject _botoes;
    public Transform canvas;

    private struct WayPoint {
        public GameObject campoDeSelecao; public Vector3 posicao; public bool passoValido;

       public WayPoint(GameObject campo, Vector3 posicaoSegura, bool validar)
        { campoDeSelecao = campo; posicao = posicaoSegura; passoValido = validar; }
    };

    static public GameObject botoes;
    GameObject objetoSelecionado;
    Vector3 posicaoDoObjetoSelecionado;
    static public bool podeChecar = false;
    static private List<GameObject> camposInstanciados = new List<GameObject>();
    static private List<TerrainCreator.Terreno> terrenos = new List<TerrainCreator.Terreno>();
    private List<WayPoint>[] locaisAlvo = new List<WayPoint>[4];

    //Variáveis globais temporárias uteis
    bool _maisAlto, _podePular = false, _podeMover = false, _caminhoCompleto;
    EnvirommentsEngine.comando _direcaoParaDestinoDoSoldado;
    float _posiY = 0;
    Vector3 _posicaoMatricialAtualDoSoldado;
    Vector3 _posicaoMatricialAlvoDoSoldado;
    Vector3[] _wayPoints;
    int _indice = 0;

    /// <summary>
    /// Capta a interface e a transforma em variáveis estáticas
    /// </summary>
    void SetPublicToStatic()
    { botoes = _botoes; }

    void Start()
    { SetPublicToStatic(); }

    void Update()
    {
        if (podeChecar)
        { ChecarObjetoSelecionado(); }

        //if (_podeMover)
        //{ TransladarSoldado(objetoSelecionado, _wayPoints); }

        if (_podeMover)
        {
            if (!_caminhoCompleto)
            {  objetoSelecionado.transform.position = Movimentos.MovimentAcrossMatrix(objetoSelecionado, _wayPoints, out _caminhoCompleto); }
            else { _podeMover = false; Movimentos.ClearData(); DestruirCaminhos(); }
        }


        if (Input.GetKeyDown(KeyCode.Return))
        { TurnControl.MudarTurno(); }
    }

    /// <summary>
    /// Checa se o objeto selecionado têm ações, se tiver desencadeia o sistema apropriado
    /// </summary>
    void ChecarObjetoSelecionado()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit choque = new RaycastHit();
            if (Physics.Raycast(raio, out choque))
            {
                Vector3 posicao = PlayerControl.ChecarAcoesDeObjeto(choque.transform.gameObject);
                if (posicao != new Vector3())
                {
                    objetoSelecionado = choque.transform.gameObject;
                    posicaoDoObjetoSelecionado = posicao;
                    Vector3 posicaoGlobal = TerrainCreator.Blocos[(int)posicao.x, (int)posicao.y, (int)posicao.z].gameObjectAcima.transform.position + new Vector3(0, 1.5f, 0);
                    _botoes.SetActive(true);
                    _botoes.transform.position = posicaoGlobal;
                }
            }
        }
    }

    static public void DesativarBotoes()
    { botoes.SetActive(false); }

    public void IniciarSistemaDeMovimentacao()
    {
        //Vai Chamar a função que inicia o sistema
        DesativarBotoes();
        DestruirCaminhos();
        DescobrirPassosEPosicaoMatricial();
    }


    void DescobrirPassosEPosicaoMatricial()
    {
        //A partir do objeto que foi selecionado anteriormente, vai descobrir a quantidade de passos que o soldado pode dar e vai chamar a função que desenha os caminhos
        string tagDoSoldado = PlayerControl.DescobrirTipoDeSoldado(objetoSelecionado);

        switch (tagDoSoldado)
        {
            case "Guerreiro":
                Soldados.Guerreiro guerreiro = new Soldados.Guerreiro();
                DesenharCaminhos(posicaoDoObjetoSelecionado, guerreiro.campoDeMovimento);
                break;

            case "Arqueiro":
                Soldados.Arqueiro arqueiro = new Soldados.Arqueiro();
                DesenharCaminhos(posicaoDoObjetoSelecionado, arqueiro.campoDeMovimento);
                break;

            case "Mago":
                Soldados.Mago mago = new Soldados.Mago();
                DesenharCaminhos(posicaoDoObjetoSelecionado, mago.campoDeMovimento);
                break;
        }
    }

    /// <summary>
    /// Desenha os possíveis caminhos para a movimentação do objeto
    /// </summary>
    /// <param name="posicaoMatricial">Posição matricial do Objeto</param>
    /// <param name="campoDeMovimento">Quantidade de passos que o objeto pode dar</param>
    void DesenharCaminhos(Vector3 posicaoMatricial, int campoDeMovimento)
    {
        /*Segue a seguinte regra:
         * 1 - A List camposInstanciados vai armazenar todos os game Objects que são instanciados para mostrar ao jogador onde ele pode mover seu soldado selecionado
         * 2 - Para cada posição coordenada (frente, tras, direita, esquerda), vai fazer uma checagem com o intuito de obter o bloco mais alto, e nesse bloco que obter, fará a seguinte checagem: 
         *      A) Se a diferença da altura entre o atual bloco e o bloco candidato for maior que 1, então esse terá um passo inválido e instanciará um campo de seleção vermelho;
         *      B) Se, no bloco candidato, houver algum obstáculo nele, então o mesmo será invalido, e instanciará um campo de seleção vermelho no mesmo;
         *      C) Caso as petições acima não sejam atendidas, o mesmo será validado, e instanciará um campo de seleção azul para mostrar que é possível um caminho no mesmo;
         *      D) Todos os campos instanciados são armazenados na List CamposInstanciados;
         * 
         * 3 - No bloco onde está o soldado selecionado, será instanciado um campo de seleção amarelo;
         */
        GameObject campo;
        float SavePosiZ = posicaoMatricial.z;
        campo = Instantiate(camposDeSelecao[2], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y, (int)posicaoMatricial.z].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
        campo.transform.parent = canvas;
        camposInstanciados.Add(campo);

        int passosDados;

        //FRENTE ----------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                //Enquanto o bloco candidato numa determinada altura nao estiver com nada acima dele, vai tentar aumentar a altura, até antender esta requisição
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    //Se em determinado bloco houver outro objeto acima senão terreno, o passo será invalidado no mesmo momento e então o loop while será quebrado
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                //checagem para garantir que nao busque níveis de blocos que não existem
                if (currentNivel > 2)
                { currentNivel = 2; }

                //Se a diferença da altura do bloco atual e a altura do bloco candidato for maior que 1, então o passo será invalido
                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                //Caso o passo seja  válido instanciará o campo de seleção azul e bla bla bla
                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel]);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[0].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, true));
                    print("ok");
                    posicaoMatricial.z = currentNivel;
                }

                //Se não for válido, o campo instanciado será o vermelho
                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[0].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, false));
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //TRÁS ----------------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                if (currentNivel > 2)
                { currentNivel = 2; }

                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[1].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, true));
                    posicaoMatricial.z = currentNivel;
                }

                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y - passosDados, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[1].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, false));
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //DIREITA -----------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                if (currentNivel > 2)
                { currentNivel = 2; }

                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[2].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, true));
                    posicaoMatricial.z = currentNivel;
                }

                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x + passosDados, (int)posicaoMatricial.y, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[2].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, false));
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //ESQUERDA ------------------------------------------------------------------------------------------------------------------------------------------------------
        for (passosDados = 1; passosDados <= campoDeMovimento; passosDados++)
        {
            int currentNivel = 0;
            bool passoValido = true;
            GameObject campoDeSelecao;
            try
            {
                while (TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.none)
                {
                    if (TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].objetoAcima != TerrainCreator.Objeto.Terreno)
                    { passoValido = false; break; }

                    currentNivel++;
                    if (currentNivel > 2)
                    { passoValido = false; break; }
                }

                if (currentNivel > 2)
                { currentNivel = 2; }

                if (((posicaoMatricial.z - currentNivel) == -2) || ((posicaoMatricial.z - currentNivel) == 2))
                { passoValido = false; }

                if (passoValido)
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[0], TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[3].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, true));
                    posicaoMatricial.z = currentNivel;
                }

                else
                {
                    campoDeSelecao = Instantiate(camposDeSelecao[3], TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel].bloco.transform.position + new Vector3(0, .52f, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                    terrenos.Add(TerrainCreator.Blocos[(int)posicaoMatricial.x - passosDados, (int)posicaoMatricial.y, currentNivel]);
                    //campoDeSelecao.transform.SetParent(canvas, true);
                    camposInstanciados.Add(campoDeSelecao);
                    locaisAlvo[3].Add(new WayPoint(campoDeSelecao, TerrainCreator.Blocos[(int)posicaoMatricial.x, (int)posicaoMatricial.y + passosDados, currentNivel].bloco.transform.position, false));
                    break;
                }
            }

            catch { break; }
        }
        posicaoMatricial.z = SavePosiZ;

        //Após desenhar os caminhos, é necessário esperar o bloco onde o Jogador quer que o soldado vá
        InvokeRepeating("EsperarPosicao", 0, Time.deltaTime);
    }

    void EsperarPosicao()
    {
        //TODO: Não esquecer de diminuir o tempo do turno por quantidade de passos;
        //TODO: Não esquecer de desmacar o podeMover do objeto após ele ter se movimentado;


        if (Input.GetMouseButtonDown(0))
        {
            Ray raio = Camera.main.camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit choque = new RaycastHit();
            if (Physics.Raycast(raio, out choque))
            {
                EnvirommentsEngine.comando direcao;
                _posicaoMatricialAlvoDoSoldado = FindBlockAndDirection(choque.transform.gameObject, out direcao);

                //Se a posição selecionada for válida, o novo alvo do Soldado será essa posição selecionada pelo jogador;
                if (_posicaoMatricialAlvoDoSoldado != new Vector3())
                {
                    Vector3 alvo = TerrainCreator.Blocos[(int)_posicaoMatricialAlvoDoSoldado.x, (int)_posicaoMatricialAlvoDoSoldado.y, (int)_posicaoMatricialAlvoDoSoldado.z].bloco.transform.position + new Vector3(0, 1.5f, 0);
                    TerrainCreator.TransferValues(posicaoDoObjetoSelecionado, _posicaoMatricialAlvoDoSoldado);
                    _posicaoMatricialAtualDoSoldado = posicaoDoObjetoSelecionado;
                    MoverSoldado(objetoSelecionado, posicaoDoObjetoSelecionado, _posicaoMatricialAlvoDoSoldado, direcao);
                    CancelInvoke("EsperarPosicao");
                }

                //Senão vai destruir os caminhos instanciados e cancelar a espera da seleção do bloco do jogador;
                else { DestruirCaminhos(); CancelInvoke("EsperarPosicao"); }
            }
        }
    }

    /// <summary>
    /// Descobre a posição matricial do bloco selecionado e a direção em que o soldado deve se mover
    /// </summary>
    /// <param name="objeto">Bloco selecionado</param>
    /// <param name="direcao">Saida da direção selecionada</param>
    /// <returns>Posição matricial do alvo</returns>
    Vector3 FindBlockAndDirection(GameObject objeto, out EnvirommentsEngine.comando direcao)
    {
        int indice = 0;
        bool blocoEncontrado = true;
        Vector3 posicao = new Vector3();
        EnvirommentsEngine.comando comando = new EnvirommentsEngine.comando();

        //Busca nas possíveis posições para moviemnto que foram salvas, o bloco selecionado;
        while (objeto != terrenos[indice].bloco)
        {
            indice++;

            //Checagem para nao haver um loop infinito caso nao seja encontrado o determinado bloco
            if (indice > terrenos.Count - 1)
            { blocoEncontrado = false; break; }
        }

        //Se o bloco for encontrado, será feito um cálculo de diferença para saber qual será direção pelo qual o soldado moverá
        if (blocoEncontrado)
        {
            posicao = TerrainCreator.FindBlockInMatrix(terrenos[indice].bloco, false);
            if (posicao != new Vector3())
            {
                int difX = (int)(posicaoDoObjetoSelecionado.x - posicao.x);
                if (difX > 0)
                { comando = EnvirommentsEngine.comando.Esquerda; }

                else if (difX < 0)
                { comando = EnvirommentsEngine.comando.Direita; }

                else
                {
                    int difY = (int)(posicaoDoObjetoSelecionado.y - posicao.y);
                    if (difY < 0)
                    { comando = EnvirommentsEngine.comando.Frente; }

                    else if (difY > 0)
                    { comando = EnvirommentsEngine.comando.Trás; }
                }
            }
        }
        direcao = comando;
        return posicao;
    }

    /// <summary>
    /// Faz o controle se pode mover o soldado ou não
    /// </summary>
    /// <param name="soldado">Soldado selecionado</param>
    /// <param name="posicaoMatricialAtual">Posição matricial atual do soldado selecionado</param>
    /// <param name="posicaoMatricialAlvo">Posição matricial onde o soldado quer ir</param>
    /// <param name="direcao">Direção pela qual o soldado vai seguir</param>
    void MoverSoldado(GameObject soldado, Vector3 posicaoMatricialAtual, Vector3 posicaoMatricialAlvo ,EnvirommentsEngine.comando direcao)
    {
        _wayPoints = GenerateWayPointSystem(locaisAlvo, direcao);

        //A variável pode mover é a variável que desencadeia a translação do soldado, ela é checada na void Update
        _podeMover = true;
    }

    /*ANTIGO SISTEMA DE TRANSLAÇÃO DO SOLDADO, TAL SISTEMA QUE DEPENDIA DE OUTRO SISTEMA JA NÃO FUNCIONA MAIS POIS SUA DEPENCIA FOI ALTERADA PARA SATISFAZER AS NECESSIDADES DO 
     SISTEMA DE TRANSLAÇÃO ATUAL*/
    //void TransladarSoldado(GameObject soldado, Vector3 posicaoMatricialAtual, EnvirommentsEngine.comando direcao)
    //{
    //    print(posicaoMatricialAtual);
    //    switch (direcao)
    //    {
    //        case EnvirommentsEngine.comando.Frente:
    //            int altura = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicaoMatricialAtual.x, posicaoMatricialAtual.y + 1));
    //            if (altura != (int)posicaoMatricialAtual.z)
    //            {
    //                if (altura < (int)posicaoMatricialAtual.z)
    //                { _maisAlto = true; _podePular = true; }

    //                else { _maisAlto = false; _podePular = true; }
    //            }

    //            if (soldado.transform.position != (TerrainCreator.Blocos[(int)posicaoMatricialAtual.x, (int)posicaoMatricialAtual.y + 1, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0)))
    //            {
    //                Vector3 coordXZ = Vector3.Lerp(soldado.transform.position, TerrainCreator.Blocos[(int)posicaoMatricialAtual.x, (int)posicaoMatricialAtual.y + 1, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0), 15000);
    //                soldado.transform.position = new Vector3(coordXZ.x, soldado.transform.position.y + _posiY, coordXZ.z);
    //            }

    //            else
    //            {
    //                _posiY = 0; _posicaoMatricialAtualDoSoldado = new Vector3(posicaoMatricialAtual.x, posicaoMatricialAtual.y + 1, posicaoMatricialAtual.z);
    //                _podeMover = false;
    //                MoverSoldado(soldado, posicaoMatricialAtual, direcao);
    //            }
    //            break;

    //        case EnvirommentsEngine.comando.Trás:
    //            altura = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicaoMatricialAtual.x, posicaoMatricialAtual.y - 1));
    //            if (altura != (int)posicaoMatricialAtual.z)
    //            {
    //                if (altura < (int)posicaoMatricialAtual.z)
    //                { _maisAlto = true; _podePular = true; }

    //                else { _maisAlto = false; _podePular = true; }
    //            }

    //            if (soldado.transform.position != (TerrainCreator.Blocos[(int)posicaoMatricialAtual.x, (int)posicaoMatricialAtual.y - 1, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0)))
    //            {
    //                Vector3 coordXZ = Vector3.Lerp(soldado.transform.position, TerrainCreator.Blocos[(int)posicaoMatricialAtual.x, (int)posicaoMatricialAtual.y - 1, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0), 15000);
    //                soldado.transform.position = new Vector3(coordXZ.x, soldado.transform.position.y + _posiY, coordXZ.z);
    //            }

    //            else
    //            {
    //                _posiY = 0; _posicaoMatricialAtualDoSoldado = new Vector3(posicaoMatricialAtual.x, posicaoMatricialAtual.y - 1, posicaoMatricialAtual.z);
    //                _podeMover = false;
    //                MoverSoldado(soldado, posicaoMatricialAtual, direcao);
    //            }
    //            break;

    //        case EnvirommentsEngine.comando.Direita:
    //            altura = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicaoMatricialAtual.x + 1, posicaoMatricialAtual.y));
    //            if (altura != (int)posicaoMatricialAtual.z)
    //            {
    //                if (altura < (int)posicaoMatricialAtual.z)
    //                { _maisAlto = true; _podePular = true; }

    //                else { _maisAlto = false; _podePular = true; }
    //            }

    //            if (soldado.transform.position != (TerrainCreator.Blocos[(int)posicaoMatricialAtual.x + 1, (int)posicaoMatricialAtual.y, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0)))
    //            {
    //                Vector3 coordXZ = Vector3.Lerp(soldado.transform.position, TerrainCreator.Blocos[(int)posicaoMatricialAtual.x + 1, (int)posicaoMatricialAtual.y, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0), 15000);
    //                soldado.transform.position = new Vector3(coordXZ.x, soldado.transform.position.y + _posiY, coordXZ.z);
    //            }

    //            else
    //            {
    //                _posiY = 0; _posicaoMatricialAtualDoSoldado = new Vector3(posicaoMatricialAtual.x + 1, posicaoMatricialAtual.y, posicaoMatricialAtual.z);
    //                _podeMover = false;
    //                MoverSoldado(soldado, posicaoMatricialAtual, direcao);
    //            }
    //            break;

    //        case EnvirommentsEngine.comando.Esquerda:
    //            altura = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicaoMatricialAtual.x - 1, posicaoMatricialAtual.y));
    //            if (altura != (int)posicaoMatricialAtual.z)
    //            {
    //                if (altura < (int)posicaoMatricialAtual.z)
    //                { _maisAlto = true; _podePular = true; }

    //                else { _maisAlto = false; _podePular = true; }
    //            }

    //            if (soldado.transform.position != (TerrainCreator.Blocos[(int)posicaoMatricialAtual.x - 1, (int)posicaoMatricialAtual.y, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0)))
    //            {
    //                Vector3 coordXZ = Vector3.Lerp(soldado.transform.position, TerrainCreator.Blocos[(int)posicaoMatricialAtual.x - 1, (int)posicaoMatricialAtual.y, (int)posicaoMatricialAtual.z].bloco.transform.position + new Vector3(0, 1.5f, 0), 15000);
    //                soldado.transform.position = new Vector3(coordXZ.x, soldado.transform.position.y + _posiY, coordXZ.z);
    //            }

    //            else
    //            {
    //                _posiY = 0; _posicaoMatricialAtualDoSoldado = new Vector3(posicaoMatricialAtual.x - 1, posicaoMatricialAtual.y, posicaoMatricialAtual.z);
    //                _podeMover = false;
    //                MoverSoldado(soldado, posicaoMatricialAtual, direcao);
    //            }
    //            break;
    //    }
    //}

    /// <summary>
    /// vai gerar um sistema de coordenadas matriciais baseada na posicao matricial atual e a posição matricial alvo
    /// </summary>
    /// <param name="posicaoMatricialAtual">posicao matricial atual do alvo</param>
    /// <param name="posicaoMatricialAlvo">posicao matricial do alvo</param>
    /// <param name="direcao">direção</param>
    /// <returns>Array de coordenadas do wayPoint</returns>
    //Vector3[] GenerateWayPointSystem(Vector3 posicaoMatricialAtual, Vector3 posicaoMatricialAlvo, EnvirommentsEngine.comando direcao)
    //{
    //    //List onde será salvas as coordenadas
    //    List<Vector3> coordenadas = new List<Vector3>();
    //    int indice = 0;
    //    float posicao = posicaoMatricialAtual.y;

    //    switch (direcao)
    //    {
    //        // Se o soldado tiver que ir para frente
    //        case EnvirommentsEngine.comando.Frente:
    //            /*Segue a seguinte regra: 
    //             * 1 - A variável posição armazena a posição mais importante do soldado, em cada caso, no caso do comando Frente, a posição mais importante é y;
    //             * 2 - O loop while checa se a posição atual é diferente da posição alvo, enquanto for diferente:
    //             *      A) A variável z vai encontrar o indice mais alto da determinada coordenada matricial XY;
    //             *      B) a List coordenadas vai armazenar a posição candidata, de forma correta; 
    //             * 3 - O mesmo é valido para os outros comenados*/
        
    //            while ((posicao) != posicaoMatricialAlvo.y)
    //            {
    //                indice++;
    //                float z = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicaoMatricialAtual.x, posicao++));
    //                print("Valor de z: " + z);
    //                coordenadas.Add(new Vector3(posicaoMatricialAtual.x, posicaoMatricialAtual.y + indice, z));
    //                posicao = posicaoMatricialAtual.y + indice;
    //                print("coordenadas salvas: " + coordenadas[coordenadas.Count-1]);
                    
    //                //Checagem de segurança para evitar um loop infinito
    //                if (indice > 7)
    //                { Debug.LogError("Ocorreu um erro inesperado!"); }
    //            }
    //            break;

    //        case EnvirommentsEngine.comando.Trás:
    //            posicao = posicaoMatricialAtual.y;
    //            while (posicao != posicaoMatricialAlvo.y)
    //            {
    //                indice++;
    //                float z = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicaoMatricialAtual.x, posicao--));
    //                coordenadas.Add(new Vector3(posicaoMatricialAtual.x, posicaoMatricialAtual.y - indice, z));
    //                posicao = posicaoMatricialAtual.y - indice;

    //                if (indice > 7)
    //                { Debug.LogError("Ocorreu um erro inesperado!"); }
    //            }
    //            break;

    //        case EnvirommentsEngine.comando.Direita:
    //            posicao = posicaoMatricialAtual.x;
    //            while (posicao != posicaoMatricialAlvo.x)
    //            {
    //                indice++;
    //                float z = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicao++, posicaoMatricialAtual.y));
    //                coordenadas.Add(new Vector3(posicaoMatricialAtual.x + indice, posicaoMatricialAtual.y, z));
    //                posicao = posicaoMatricialAtual.x + indice;

    //                if (indice > 7)
    //                { Debug.LogError("Ocorreu um erro inesperado!"); }
    //            }
    //            break;

    //        case EnvirommentsEngine.comando.Esquerda:
    //            posicao = posicaoMatricialAtual.x;
    //            while (posicao != posicaoMatricialAlvo.x)
    //            {
    //                indice++;
    //                float z = TerrainCreator.CheckMaxIndexInBlock(new Vector2(posicao--, posicaoMatricialAtual.y));
    //                coordenadas.Add(new Vector3(posicaoMatricialAtual.x - indice, posicaoMatricialAtual.y, z));
    //                posicao = posicaoMatricialAtual.x - indice;

    //                if (indice > 7)
    //                { Debug.LogError("Ocorreu um erro inesperado!"); }
    //            }
    //            break;

    //        default:
    //            Debug.LogError("O Comando não foi reconhecido!");
    //            break;
    //    }
    //    return coordenadas.ToArray();
    //}

    Vector3[] GenerateWayPointSystem(List<WayPoint>[] carretelDeWays, EnvirommentsEngine.comando direcao)
    {
        WayPoint[] pointsWay;
        List<Vector3> points = new List<Vector3>();
        switch (direcao)
        {
            case EnvirommentsEngine.comando.Frente:
                pointsWay = carretelDeWays[0].ToArray();
                for (int i = 0; i < pointsWay.Length - 1; i++)
                { points.Add(pointsWay[i].posicao); }
                break;

            case EnvirommentsEngine.comando.Trás:
                pointsWay = carretelDeWays[1].ToArray();
                for (int i = 0; i < pointsWay.Length - 1; i++)
                { points.Add(pointsWay[i].posicao); }
                break;

            case EnvirommentsEngine.comando.Direita:
                pointsWay = carretelDeWays[2].ToArray();
                for (int i = 0; i < pointsWay.Length - 1; i++)
                { points.Add(pointsWay[i].posicao); }
                break;

            case EnvirommentsEngine.comando.Esquerda:
                pointsWay = carretelDeWays[3].ToArray();
                for (int i = 0; i < pointsWay.Length - 1; i++)
                { points.Add(pointsWay[i].posicao); }
                break;
        }

        return points.ToArray();
    }

    /// <summary>
    /// Sistema que movimenta o Game Object soldado
    /// </summary>
    /// <param name="soldado">Soldado a ser movimentado</param>
    /// <param name="wayPoints">Sistema de WayPoint por onde passrá o soldado</param>
    //void TransladarSoldado(GameObject soldado, Vector3[] wayPoints )
    //{
    //    Vector3 posicaoGlobalAlvo;
    //    try
    //    {
    //        posicaoGlobalAlvo = TerrainCreator.Blocos[(int)wayPoints[_indice].x, (int)wayPoints[_indice].y, (int)wayPoints[_indice].z].bloco.transform.position + new Vector3(0, 1.5f, 0);
    //        soldado.transform.position = Vector3.Slerp(soldado.transform.position, posicaoGlobalAlvo, 10*Time.deltaTime);
    //        if (soldado.transform.position == posicaoGlobalAlvo)
    //        { _indice++; }
    //    }

    //    catch { Debug.Log("Trajeto Terminado!"); _podeMover = false; DestruirCaminhos(); }
    //}



    float maxTimeDelta = 0;
    //Sistema de pulo atualmente não está sendo utilizado pois foi substituido por Vector3.Slerp, mas é provável que será utilizado mais pra frente por ser algo mais voltado à movimentação de pulo
    void Pular()
    {
        //função que descreve o pulo F(x) = -4X² + 4X
        if (_maisAlto)
        {
            maxTimeDelta += Time.deltaTime * 2;
            if (maxTimeDelta > .5f)
            { maxTimeDelta = .5f; }
            _posiY = (-4 * Mathf.Pow(maxTimeDelta, 2)) + (4 * maxTimeDelta);

            if (maxTimeDelta >= .5f)
            { maxTimeDelta = .5f; _posiY = 1; _podePular = false; maxTimeDelta = 0; }
        }

        else
        {
            maxTimeDelta += Time.deltaTime * 2;
            if (maxTimeDelta > 1.2f)
            { maxTimeDelta = 1.2f; }
            _posiY = (-4 * Mathf.Pow(maxTimeDelta, 2)) + (4 * maxTimeDelta);

            if (maxTimeDelta >= 1.2f)
            { maxTimeDelta = 1.2f; _posiY = -1f; _podePular = false; maxTimeDelta = 0; }
        }
    }

    static public void DestruirCaminhos()
    {
        foreach (GameObject objeto in camposInstanciados)
        { Destroy(objeto); }
        camposInstanciados.Clear();

        terrenos.Clear();
        print("Valores Destruidos");
    }
}