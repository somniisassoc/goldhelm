﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Banco de Informacoes dos soldados e tenda
/// </summary>
public class Soldados : MonoBehaviour {

    /// <summary>
    /// Acao tomada pelos soldados, é mais fraca mas demanda menos tempo
    /// </summary>
    public struct AcaoFraca
    {
        readonly int forca;
        readonly int tempo;

        public AcaoFraca(int atk, int steps)
        {
            forca = atk;
            tempo = steps;
        }
    }
    /// <summary>
    /// Acao tomada pelo soldados, tem uma forca média e portanto, demanda um tempo superior à acao fraca
    /// </summary>
    public struct AcaoMedia
    {
        readonly int forca;
        readonly int tempo;

        public AcaoMedia(int atk, int steps)
        {
            forca = atk;
            tempo = steps;
        }
    }
    /// <summary>
    /// Acao tomada pelos soldados,é a mais forte e também a que utiliza mais tempo de execucao
    /// </summary>
    public struct AcaoForte
    {
        readonly int forca;
        readonly int tempo;

        public AcaoForte(int atk, int steps)
        {
            forca = atk;
            tempo = steps;
        }
    }

    public enum Equipe { Equipe1 = 0, Equipe2 };

    public class Guerreiro
    {
        public int energia = 100;
        public readonly int campoDeVisao = 1;
        public readonly int forcaDeAtaque = 29;
        public readonly int forcaDeDefesa = 6;
        public readonly int sorte = 13;
        public readonly int preco = 390;
        public readonly int campoDeMovimento = 1;
        public readonly string definicao = "Nobre soldado que está na linha de frente, com uma força muito grande mas um campo de visão pequeno";

        public GameObject Objeto;
        public bool podeMover = false;
        public AcaoFraca ataqueFraco = new AcaoFraca(10,2);
        public AcaoMedia ataqueMedio = new AcaoMedia(17,4);
        public AcaoForte ataqueForte = new AcaoForte(22,7);
        public Equipe equipe;
    }

    public class Arqueiro
    {
        public int energia = 60;
        public readonly int campoDeVisao = 3;
        public readonly int forcaDeAtaque = 19;
        public readonly int forcaDeDefesa = 4;
        public readonly int sorte = 8;
        public readonly int preco = 575;
        public readonly int campoDeMovimento = 2;
        public readonly string definicao = "Apesar de não ser tão forte, é capaz de atacar de longe, o que lhe dar uma grande vantagem";

        public GameObject Objeto;
        public bool podeMover = false;
        public AcaoFraca ataqueFraco = new AcaoFraca(10, 2);
        public AcaoMedia ataqueMedio = new AcaoMedia(17, 4);
        public AcaoForte ataqueForte = new AcaoForte(22, 7);
        public Equipe equipe;
    }

    public class Mago
    {
        public int energia = 40;
        public readonly int campoDeVisao = 1;
        public readonly int forcaDeAtaque = 12;
        public readonly int forcaDeDefesa = 3;
        public readonly int sorte = 20;
        public readonly int preco = 750;
        public readonly int campoDeMovimento = 2;
        public readonly string definicao = "Não ataca os inimigos, mas é capaz de revitalizar todos os soldados, dando uma força grande na hora do confronto";

        public GameObject Objeto;
        public bool podeMover = false;
        public AcaoFraca ataqueFraco = new AcaoFraca(10, 2);
        public AcaoMedia ataqueMedio = new AcaoMedia(17, 4);
        public AcaoForte ataqueForte = new AcaoForte(22, 7);
        public Equipe equipe;
    }

    public class Tenda
    {
        public int energia = 600;
        public readonly string definicao = "Tenda onde é possível comprar e vender soldados. Sem a mesma, não ha como adicionar mais soldados à batalha";

        public TerrainCreator.Objeto tipoDeTenda;
        public GameObject objeto;
        public bool podeMover = false;
        public Equipe equipe;
    }
}
