﻿using UnityEngine;
using System.Collections;

public class Movimentos : MonoBehaviour
{

    static private int index = 0;
    static public Vector3 MovimentAcrossMatrix(GameObject soldado, Vector3[] wayPoints, out bool processoCompleto)
    {
        Vector3 posicao = new Vector3();
        bool caminhoCompletado = false;
        posicao = Vector3.Slerp(soldado.transform.position, TerrainCreator.Blocos[(int)wayPoints[index].x, (int)wayPoints[index].y, (int)wayPoints[index].z].bloco.transform.position + new Vector3(0, 1.5f, 0), 10 * Time.deltaTime);

        if(IsSuficientNear(posicao,TerrainCreator.Blocos[(int)wayPoints[index].x, (int)wayPoints[index].y, (int)wayPoints[index].z].bloco.transform.position + new Vector3(0, 1.5f, 0)))
        { 
            if (index < wayPoints.Length - 1) { index++; }
            else { caminhoCompletado = true; }
        }

        processoCompleto = caminhoCompletado;
        return posicao;
    }

    static private bool IsSuficientNear(Vector3 posicaoAtual, Vector3 posicaoAlvo)
    {
        if (Vector3.Distance(posicaoAtual, posicaoAlvo) < .03f)
        { return true; }
        else { return false; }
    }

    static public void ClearData()
    { index = 0; }
}
