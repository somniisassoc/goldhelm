﻿using UnityEngine;
using System.Collections;

public class Stepped : MonoBehaviour {

    /// <summary>
    /// A principal variável, ela quem vai comandar todo o sistema
    /// </summary>
    protected int step { get; private set; }

    protected GameObject objetoSelecionado { get; private set; }


    /// <summary>
    /// Inicializa o sistema de passos, deve ser declarada antes de trabalhar com o sistema
    /// </summary>
    protected void StartSystem()
    { step = 0; }

    protected void NextStep(string function)
    { step++; Invoke(function, 0); }

    protected void SetObject(GameObject objeto)
    {
        if (objeto != null) objetoSelecionado = objeto;
        else objetoSelecionado = new GameObject();
    }

    protected void PlayStep(string function, int passo)
    { step = passo; Invoke(function, 0); }
}
