TERRAIN CREATOR ENGINE VERSION 4.1.5

::::Script responsavel pela cria��o e controle de todos os blocos matriciais do jogo::::
----------------------------------------------------------------------------------------

-- NOVIDADES --
* fun��o FindObjectinMatrix agora se chama FindblockInMatrix, o que a torna mais intuitiva e dar lugar � nova fun��o implementada;
* fun��o FindGameObjectInMatrix busca posi��o de objetos na matriz;

-- BUGS ENCONTRADOS --
* A fun��o FindBlockInMatrix n�o retorna a posi��o (0,0,0);